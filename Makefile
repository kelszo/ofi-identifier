conda_create:
	micromamba create --prefix ./env --file ./environment.yml

conda_update:
	micromamba update --prefix ./env --prune --file ./environment.yml

conda_clean:
	micromamba clean --all