{ pkgs ? import <nixpkgs> }:

with pkgs { };

mkShell {
  name = "ofiidentifier-env";
  buildInputs = [
    python39
    python39Packages.venvShellHook
    autoPatchelfHook
  ];
  propagatedBuildInputs = [
    stdenv.cc.cc.lib
    zlib
  ];

  venvDir = "./venv";
  postVenvCreation = ''
    unset SOURCE_DATE_EPOCH
    mkdir $PWD/tmp
    export TMPDIR=$PWD/tmp
    pip install -U pip setuptools wheel
    pip install -r requirements.txt
    autoPatchelf ./venv
    export TMPDIR=/tmp
    rm -rf $PWD/tmp
  '';

  # LD_LIBRARY_PATH = "${pkgs.stdenv.cc.cc.lib}/lib:$LD_LIBRARY_PATH";

  postShellHook = ''
    # set SOURCE_DATE_EPOCH so that we can use python wheels
    export SOURCE_DATE_EPOCH=315532800
    unset LD_LIBRARY_PATH
  '';
  preferLocalBuild = true;
}
