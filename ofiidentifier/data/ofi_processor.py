import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler, power_transform

default_columns = [
    "pt_age_yrs",
    "pt_Gender",
    "inj_dominant",
    "inj_mechanism",
    "inj_intention",
    "pt_asa_preinjury",
    "pre_card_arrest",
    "pre_gcs_sum",
    "ed_gcs_sum",
    "pre_sbp_value",
    "ed_sbp_value",
    "pre_rr_value",
    "ed_rr_value",
    "ed_be_art",
    "ed_inr",
    "hosp_vent_days",
    "hosp_los_days",
    "hosp_dischg_dest",
    "res_gos_dischg",
    "res_survival",
    "TraumaAlarmCriteria",
    "TraumaAlarmAtHospital",
    "AlarmRePrioritised",
    "ISS",
    "dt_alarm_hosp",
    "pre_provided",
    "pre_intubated",
    "pre_intub_type",
    "ed_intubated",
    "ed_intub_type",
    "pre_transport",
    "ed_emerg_proc",
    "ed_emerg_proc_other",
    "ed_tta",
    "host_transfered",
    "host_care_level",
    "dt_alarm_scene",
    "dt_ed_first_ct",
    "dt_ed_emerg_proc",
    "NumberOfActions",
    "NumberOfInjuries",
    "pac_code_*",
    "ICD_*",
    "AISCode_*",
    "Tr_Niva",
    "iva_dagar_n",
    "iva_vardtillfallen_n",
    "waran_beh_vid_ank",
    "noak_vid_ankomst",
    "VK_*",
    "ofi",
]

one_hot_columns = [
    "pt_Gender",
    "inj_dominant",
    "inj_mechanism",
    "inj_intention",
    "pt_asa_preinjury",
    "pre_card_arrest",
    "hosp_dischg_dest",
    "res_gos_dischg",
    "res_survival",
    "TraumaAlarmCriteria",
    "TraumaAlarmAtHospital",
    "AlarmRePrioritised",
    "pre_provided",
    "pre_intubated",
    "pre_intub_type",
    "ed_intubated",
    "ed_intub_type",
    "pre_transport",
    "ed_emerg_proc",
    "ed_emerg_proc_other",
    "ed_tta",
    "host_transfered",
    "host_care_level",
    "Tr_Niva",
]

# All codes above 1% of the data
ais_codes = [
    110202.1,
    110402.1,
    110602.1,
    140605.2,
    140651.3,
    140652.4,
    140656.5,
    140675.2,
    140682.3,
    140694.2,
    140695.3,
    150202.3,
    150402.2,
    161001.1,
    161004.2,
    210202.1,
    210402.1,
    210602.1,
    250800.2,
    251000.1,
    251002.2,
    251006.2,
    251221.2,
    251235.2,
    251404.1,
    310402.1,
    410202.1,
    410402.1,
    410602.1,
    441407.2,
    441411.3,
    441431.3,
    442200.3,
    442202.2,
    442205.3,
    450201.1,
    450202.2,
    450203.3,
    450212.3,
    450804.2,
    510202.1,
    510402.1,
    510602.1,
    640284.1,
    650217.2,
    650417.2,
    650420.2,
    650432.2,
    650617.2,
    650620.2,
    650632.2,
    710202.1,
    710402.1,
    710602.1,
    750651.2,
    750671.2,
    750951.2,
    752371.2,
    810202.1,
    810402.1,
    810602.1,
    853151.3,
    854471.2,
    856151.2,
    856163.4,
    910400.1,
]

icd_codes = [
    "S00.0",
    "S00.8",
    "S01.0",
    "S01.8",
    "S02.00",
    "S02.10",
    "S02.2",
    "S02.30",
    "S02.4",
    "S02.40",
    "S02.5",
    "S02.80",
    "S06.0",
    "S06.3",
    "S06.4",
    "S06.5",
    "S06.6",
    "S06.8",
    "S10.8",
    "S10.9",
    "S11.8",
    "S12.2",
    "S13.4",
    "S14.0",
    "S20.8",
    "S21.9",
    "S22.0",
    "S22.20",
    "S22.30",
    "S22.40",
    "S27.00",
    "S27.20",
    "S27.21",
    "S27.30",
    "S30.8",
    "S30.9",
    "S32.0",
    "S32.40",
    "S32.70",
    "S32.80",
    "S36.00",
    "S36.10",
    "S37.00",
    "S42.00",
    "S42.10",
    "S42.20",
    "S52.50",
    "S71.1",
    "S72.10",
    "S82.4",
    "T08",
    "T09",
    "T11.0",
    "T11.1",
    "T13.0",
    "T13.1",
    "T14.0",
    "T14.1",
    "T68",
]

pac_codes = [
    "AAA20",
    "AAD05",
    "DQ023",
    "EAB00",
    "GAA10",
    "GBB00",
    "JAH00",
    "NAG49",
    "NAG79",
    "NCJ19",
    "NCJ69",
    "NEJ19",
    "NEJ69",
    "NFJ09",
    "NFJ59",
    "NGJ09",
    "NGJ19",
    "NGJ29",
    "QAB00",
    "QBB00",
    "QCB00",
    "QCB05",
    "QDB00",
    "QDB05",
    "TNA31",
    "TNB30",
    "TNC32",
    "TND32",
    "TNG31",
    "TNG32",
    "TNH32",
]


class OFIProcessor:
    def __init__(self, df_raw: pd.DataFrame, impute: bool, do_one_hot_encode: bool, rescale: str):
        self.df_raw = df_raw.copy()
        self.df_raw.rename(columns={"Tr_Nivå": "Tr_Niva"}, inplace=True)
        self.df_raw.drop_duplicates(subset=["id"], inplace=True)
        self.df_raw = self.df_raw.loc[self.df_raw["SweTr"] == "Ja"]
        self.df_raw.set_index("id", inplace=True)
        self.impute = impute
        self.do_one_hot_encode = do_one_hot_encode
        self.rescale = rescale

        self.df_processed = pd.DataFrame(index=self.df_raw.index)

        self.categories = {}
        self.gaussian_scalars = []
        self.non_gaussian_scalars = []

        for column in self.df_raw.columns:
            if column.startswith("VK_"):
                self._vk_all(column)
            else:
                try:
                    getattr(self, f"_{column}")()
                except:
                    pass

            self.df_processed = self.df_processed.copy()

        pac_columns = [x for x in self.df_raw.columns if x.startswith("pac_code_")]
        icd_columns = [x for x in self.df_raw.columns if x.startswith("ICD_")]
        ais_columns = [x for x in self.df_raw.columns if x.startswith("AISCode_")]

        for pac_code in pac_codes:
            self.categories[f"pac_{pac_code}"] = 2
            self.df_processed[f"pac_{pac_code}"] = (self.df_raw[pac_columns] == pac_code).sum(axis=1).clip(0, 1)
            self.df_processed = self.df_processed.copy()

        for icd_code in icd_codes:
            self.categories[f"icd_{icd_code}"] = 2
            self.df_processed[f"icd_{icd_code}"] = (self.df_raw[icd_columns] == icd_code).sum(axis=1).clip(0, 1)
            self.df_processed = self.df_processed.copy()

        for ais_code in ais_codes:
            self.categories[f"ais_{ais_code}"] = 2
            self.df_processed[f"ais_{ais_code}"] = (self.df_raw[ais_columns] == ais_code).sum(axis=1).clip(0, 1)
            self.df_processed = self.df_processed.copy()

        self.filters()

        if self.do_one_hot_encode:
            self.df_processed = self.one_hot_encode(self.df_processed)

        if self.rescale == "standardise":
            self.df_processed[self.gaussian_scalars + self.non_gaussian_scalars] = power_transform(
                self.df_processed[self.gaussian_scalars + self.non_gaussian_scalars]
            )
        elif self.rescale == "normalise":
            self.df_processed[self.non_gaussian_scalars] = MinMaxScaler().fit_transform(
                self.df_processed[self.non_gaussian_scalars]
            )

    def one_hot_encode(self, X: pd.DataFrame) -> pd.DataFrame:
        curr_one_hot_columns = np.intersect1d(X.columns, one_hot_columns)

        one_hot_df = X[curr_one_hot_columns]
        one_hot_df = pd.get_dummies(one_hot_df, columns=curr_one_hot_columns)
        score_one_hot_drop = X.drop(curr_one_hot_columns, axis=1)
        X = pd.concat([score_one_hot_drop, one_hot_df], axis=1)

        return X

    def __encode_category(self, column: str):
        if self.impute:
            self.df_processed[column].replace(999, 0, inplace=True)
            self.df_processed[column].fillna(0, inplace=True)
        else:
            self.df_processed[column].replace(999, np.NaN, inplace=True)

        values = np.sort(self.df_processed[column].unique())

        # fix category outliers for TabNet: sort out 99 and 9999 codes
        for (i, value) in enumerate(values):
            if value != i:
                self.df_processed[column].replace(value, i, inplace=True)

        self.categories[column] = self.df_processed[column].nunique()

    def __encode_yes_no(self, column: str):
        self.df_processed[column].replace("(?i)Nej", 0, inplace=True, regex=True)
        self.df_processed[column].replace("(?i)nn", 0, inplace=True, regex=True)
        self.df_processed[column].replace("(?i)nj", 0, inplace=True, regex=True)
        self.df_processed[column].replace("(?i)No", 0, inplace=True, regex=True)

        self.df_processed[column].replace("(?i)Ja", 1, inplace=True, regex=True)
        self.df_processed[column].replace("(?i)Yes", 1, inplace=True, regex=True)

        self.df_processed[column].fillna(0, inplace=True)

        self.categories[column] = self.df_processed[column].nunique()

    def filters(self):
        self.df_processed["filter_iss_15_no_iva"] = (
            (self.df_raw["ISS"] > 15) & (self.df_raw["host_care_level"] != 5)
        ).astype(int)
        self.categories["filter_iss_15_no_iva"] = 2

        self.df_processed["filter_dt_delay"] = (self.df_raw["dt_ed_first_ct"] > 30).astype(int)
        self.categories["filter_dt_delay"] = 2

        self.df_processed["filter_intervention_delay"] = (self.df_raw["dt_ed_emerg_proc"] > 60).astype(int)
        self.categories["filter_intervention_delay"] = 2

        self.df_processed["filter_iss_15_no_tta"] = ((self.df_raw["ISS"] > 15) & (self.df_raw["ed_tta"] != 2)).astype(
            int
        )
        self.categories["filter_iss_15_no_tta"] = 2

    def _pt_age_yrs(self):
        self.df_processed["pt_age_yrs"] = self.df_raw["pt_age_yrs"]

        if self.impute:
            self.df_processed["pt_age_yrs"].fillna(self.df_processed["pt_age_yrs"].median(), inplace=True)

        self.non_gaussian_scalars.append("pt_age_yrs")

    def _pt_Gender(self):
        self.df_processed["pt_Gender"] = self.df_raw["pt_Gender"]

        self.__encode_category("pt_Gender")

    def _inj_dominant(self):
        self.df_processed["inj_dominant"] = self.df_raw["inj_dominant"]

        self.__encode_category("inj_dominant")

    def _inj_mechanism(self):
        self.df_processed["inj_mechanism"] = self.df_raw["inj_mechanism"]

        self.__encode_category("inj_mechanism")

    def _inj_intention(self):
        self.df_processed["inj_intention"] = self.df_raw["inj_intention"]

        self.__encode_category("inj_intention")

    def _pt_asa_preinjury(self):
        self.df_processed["pt_asa_preinjury"] = self.df_raw["pt_asa_preinjury"]

        self.__encode_category("pt_asa_preinjury")

    def _pre_card_arrest(self):
        self.df_processed["pre_card_arrest"] = self.df_raw["pre_card_arrest"]

        self.df_processed["pre_card_arrest"].replace(999, np.NaN, inplace=True)

        if self.impute:
            self.df_processed["pre_card_arrest"].fillna(2, inplace=True)

    def _pre_gcs_sum(self):
        self.df_processed["pre_gcs_sum"] = self.df_raw["pre_gcs_sum"]

        self.df_processed["pre_gcs_sum"].replace(999, np.NaN, inplace=True)
        if self.impute:
            self.df_processed["pre_gcs_sum"].fillna(self.df_processed["pre_gcs_sum"].median(), inplace=True)

    def _ed_gcs_sum(self):
        self.df_processed["ed_gcs_sum"] = self.df_raw["ed_gcs_sum"]

        self.df_processed["ed_gcs_sum"].replace(99, np.NaN, inplace=True)
        self.df_processed["ed_gcs_sum"].replace(999, np.NaN, inplace=True)

        # Replace ed_gcs_sum with pre_gcs_sum where ed_gcs_sum is na
        # self.df_processed.loc[self.df_processed.ed_gcs_sum.isna(), "ed_gcs_sum"] = self.df_processed.loc[self.df_processed["ed_gcs_sum"].isna()]["pre_gcs_sum"]
        if self.impute:
            self.df_processed["ed_gcs_sum"].fillna(self.df_processed["ed_gcs_sum"].median(), inplace=True)

    def _pre_sbp_value(self):
        self.df_processed["pre_sbp_value"] = self.df_raw["pre_sbp_value"]

        self.df_processed.loc[self.df_raw["pre_sbp_rtscat"] == 4, "pre_sbp_value"] = 100
        self.df_processed.loc[self.df_raw["pre_sbp_rtscat"] == 3, "pre_sbp_value"] = 83
        self.df_processed.loc[self.df_raw["pre_sbp_rtscat"] == 2, "pre_sbp_value"] = 62
        self.df_processed.loc[self.df_raw["pre_sbp_rtscat"] == 1, "pre_sbp_value"] = 25
        self.df_processed.loc[self.df_raw["pre_sbp_rtscat"] == 0, "pre_sbp_value"] = 0

        if self.impute:
            self.df_processed["pre_sbp_value"].fillna(self.df_processed["pre_sbp_value"].median(), inplace=True)

        self.gaussian_scalars.append("pre_sbp_value")

    def _ed_sbp_value(self):
        self.df_processed["ed_sbp_value"] = self.df_raw["ed_sbp_value"]

        self.df_processed.loc[self.df_raw["ed_sbp_rtscat"] == 4, "ed_sbp_value"] = 100
        self.df_processed.loc[self.df_raw["ed_sbp_rtscat"] == 3, "ed_sbp_value"] = 83
        self.df_processed.loc[self.df_raw["ed_sbp_rtscat"] == 2, "ed_sbp_value"] = 62
        self.df_processed.loc[self.df_raw["ed_sbp_rtscat"] == 1, "ed_sbp_value"] = 25
        self.df_processed.loc[self.df_raw["ed_sbp_rtscat"] == 0, "ed_sbp_value"] = 0

        if self.impute:
            self.df_processed["ed_sbp_value"].fillna(self.df_processed["ed_sbp_value"].median(), inplace=True)

        self.gaussian_scalars.append("ed_sbp_value")

    def _pre_rr_value(self):
        self.df_processed["pre_rr_value"] = self.df_raw["pre_rr_value"]

        self.df_processed.loc[self.df_raw["pre_rr_rtscat"] == 4, "pre_rr_value"] = 15
        self.df_processed.loc[self.df_raw["pre_rr_rtscat"] == 3, "pre_rr_value"] = 35
        self.df_processed.loc[self.df_raw["pre_rr_rtscat"] == 2, "pre_rr_value"] = 7
        self.df_processed.loc[self.df_raw["pre_rr_rtscat"] == 1, "pre_rr_value"] = 3
        self.df_processed.loc[self.df_raw["pre_rr_rtscat"] == 0, "pre_rr_value"] = 0

        if self.impute:
            self.df_processed["pre_rr_value"].fillna(self.df_processed["pre_rr_value"].median(), inplace=True)

        self.gaussian_scalars.append("pre_rr_value")

    def _ed_rr_value(self):
        self.df_processed["ed_rr_value"] = self.df_raw["ed_rr_value"]

        self.df_processed["ed_rr_value"].replace(99, np.NaN, inplace=True)
        self.df_processed.loc[self.df_raw["ed_rr_rtscat"] == 4, "ed_rr_value"] = 15
        self.df_processed.loc[self.df_raw["ed_rr_rtscat"] == 3, "ed_rr_value"] = 35
        self.df_processed.loc[self.df_raw["ed_rr_rtscat"] == 2, "ed_rr_value"] = 7
        self.df_processed.loc[self.df_raw["ed_rr_rtscat"] == 1, "ed_rr_value"] = 3
        self.df_processed.loc[self.df_raw["ed_rr_rtscat"] == 0, "ed_rr_value"] = 0

        if self.impute:
            self.df_processed["ed_rr_value"].fillna(self.df_processed["ed_rr_value"].median(), inplace=True)

        self.gaussian_scalars.append("ed_rr_value")

    def _ed_be_art(self):
        self.df_processed["ed_be_art"] = pd.to_numeric(self.df_raw["ed_be_art"].str.replace(",", "."))
        self.df_processed["ed_be_art_NotDone"] = self.df_raw["ed_be_art_NotDone"]

        self.df_processed["ed_be_art_NotDone"].fillna(1, inplace=True)
        if self.impute:
            self.df_processed["ed_be_art"].fillna(self.df_processed["ed_be_art"].median(), inplace=True)

        self.gaussian_scalars.append("ed_be_art")

    def _ed_inr(self):
        self.df_processed["ed_inr"] = pd.to_numeric(self.df_raw["ed_inr"].str.replace(",", "."))
        self.df_processed["ed_inr_NotDone"] = self.df_raw["ed_be_art_NotDone"]

        self.df_processed["ed_inr_NotDone"].fillna(1, inplace=True)
        if self.impute:
            self.df_processed["ed_inr"].fillna(self.df_processed["ed_inr"].median(), inplace=True)

        self.gaussian_scalars.append("ed_inr")

    def _hosp_vent_days(self):
        self.df_processed["hosp_vent_days"] = self.df_raw["hosp_vent_days"]
        self.df_processed["host_vent_days_NotDone"] = self.df_raw["host_vent_days_NotDone"]

        self.df_processed["host_vent_days_NotDone"].fillna(1, inplace=True)
        if self.impute:
            self.df_processed["hosp_vent_days"].fillna(0, inplace=True)

        self.__encode_category("host_vent_days_NotDone")

        self.non_gaussian_scalars.append("hosp_vent_days")

    def _hosp_los_days(self):
        self.df_processed["hosp_los_days"] = self.df_raw["hosp_los_days"]

        if self.impute:
            self.df_processed["hosp_los_days"].fillna(self.df_processed["hosp_los_days"].median(), inplace=True)

        self.non_gaussian_scalars.append("hosp_los_days")

    def _hosp_dischg_dest(self):
        self.df_processed["hosp_dischg_dest"] = self.df_raw["hosp_dischg_dest"]

        self.__encode_category("hosp_dischg_dest")

    def _res_gos_dischg(self):
        self.df_processed["res_gos_dischg"] = self.df_raw["res_gos_dischg"]

        self.__encode_category("res_gos_dischg")

    def _res_survival(self):
        self.df_processed["res_survival"] = self.df_raw["res_survival"]

        self.__encode_category("res_survival")

    def _TraumaAlarmCriteria(self):
        self.df_processed["TraumaAlarmCriteria"] = self.df_raw["TraumaAlarmCriteria"]

        self.__encode_category("TraumaAlarmCriteria")

    def _TraumaAlarmAtHospital(self):
        self.df_processed["TraumaAlarmAtHospital"] = self.df_raw["TraumaAlarmAtHospital"]

        self.__encode_category("TraumaAlarmAtHospital")

    def _AlarmRePrioritised(self):
        self.df_processed["AlarmRePrioritised"] = self.df_raw["AlarmRePrioritised"]

        self.__encode_category("AlarmRePrioritised")

    def _ISS(self):
        self.df_processed["ISS"] = self.df_raw["ISS"]

        if self.impute:
            self.df_processed["ISS"].fillna(self.df_processed["ISS"].median(), inplace=True)

        self.non_gaussian_scalars.append("ISS")

    def _dt_alarm_hosp(self):
        self.df_processed["dt_alarm_hosp"] = self.df_raw["dt_alarm_hosp"]

        if self.impute:
            self.df_processed["dt_alarm_hosp"].fillna(self.df_processed["dt_alarm_hosp"].median(), inplace=True)

        self.gaussian_scalars.append("dt_alarm_hosp")

    def _pre_provided(self):
        self.df_processed["pre_provided"] = self.df_raw["pre_provided"]

        self.__encode_category("pre_provided")

    def _pre_intubated(self):
        self.df_processed["pre_intubated"] = self.df_raw["pre_intubated"]

        self.__encode_category("pre_intubated")

    def _pre_intub_type(self):
        self.df_processed["pre_intub_type"] = self.df_raw["pre_intub_type"]

        self.__encode_category("pre_intub_type")

    def _ed_intubated(self):
        self.df_processed["ed_intubated"] = self.df_raw["ed_intubated"]

        self.__encode_category("ed_intubated")

    def _ed_intub_type(self):
        self.df_processed["ed_intub_type"] = self.df_raw["ed_intub_type"]

        self.__encode_category("ed_intub_type")

    def _pre_transport(self):
        self.df_processed["pre_transport"] = self.df_raw["pre_transport"]

        self.__encode_category("pre_transport")

    def _ed_emerg_proc(self):
        self.df_processed["ed_emerg_proc"] = self.df_raw["ed_emerg_proc"]

        self.__encode_category("ed_emerg_proc")

    def _ed_emerg_proc_other(self):
        self.df_processed["ed_emerg_proc_other"] = self.df_raw["ed_emerg_proc_other"]

        self.__encode_category("ed_emerg_proc_other")

    def _ed_emerg_proc_other(self):
        self.df_processed["ed_emerg_proc_other"] = self.df_raw["ed_emerg_proc_other"]

        self.__encode_category("ed_emerg_proc_other")

    def _ed_tta(self):
        self.df_processed["ed_tta"] = self.df_raw["ed_tta"]

        self.__encode_category("ed_tta")

    def _host_transfered(self):
        self.df_processed["host_transfered"] = self.df_raw["host_transfered"]

        self.__encode_category("host_transfered")

    def _host_care_level(self):
        self.df_processed["host_care_level"] = self.df_raw["host_care_level"]

        self.__encode_category("host_care_level")

    def _dt_alarm_scene(self):
        self.df_processed["dt_alarm_scene"] = self.df_raw["dt_alarm_scene"]

        if self.impute:
            self.df_processed["dt_alarm_scene"].fillna(self.df_processed["dt_alarm_scene"].median(), inplace=True)

        self.gaussian_scalars.append("dt_alarm_scene")

    def _dt_ed_first_ct(self):
        self.df_processed["dt_ed_first_ct"] = self.df_raw["dt_ed_first_ct"]
        self.df_processed["FirstTraumaDT_NotDone"] = self.df_raw["FirstTraumaDT_NotDone"]

        self.df_processed["FirstTraumaDT_NotDone"].fillna(1, inplace=True)

        if self.impute:
            self.df_processed["dt_ed_first_ct"].fillna(self.df_processed["dt_ed_first_ct"].median(), inplace=True)

        self.__encode_category("FirstTraumaDT_NotDone")

        self.gaussian_scalars.append("dt_ed_first_ct")

    def _dt_ed_emerg_proc(self):
        self.df_processed["dt_ed_emerg_proc"] = self.df_raw["dt_ed_emerg_proc"]

        if self.impute:
            self.df_processed["dt_ed_emerg_proc"].fillna(self.df_processed["dt_ed_emerg_proc"].median(), inplace=True)

        self.gaussian_scalars.append("dt_ed_emerg_proc")

    def _NumberOfActions(self):
        self.df_processed["NumberOfActions"] = self.df_raw["NumberOfActions"]

        if self.impute:
            self.df_processed["NumberOfActions"].fillna(self.df_processed["NumberOfActions"].median(), inplace=True)

        self.gaussian_scalars.append("NumberOfActions")

    def _NumberOfInjuries(self):
        self.df_processed["NumberOfInjuries"] = self.df_raw["NumberOfInjuries"]

        if self.impute:
            self.df_processed["NumberOfInjuries"].fillna(self.df_processed["NumberOfInjuries"].median(), inplace=True)

        self.gaussian_scalars.append("NumberOfInjuries")

    def _Tr_Niva(self):
        self.df_processed["Tr_Niva"] = self.df_raw["Tr_Niva"]

        self.df_processed["Tr_Niva"].replace(33, 3, inplace=True)

    def _iva_dagar_n(self):
        self.df_processed["iva_dagar_n"] = self.df_raw["iva_dagar_n"]

        if self.impute:
            self.df_processed["iva_dagar_n"].fillna(0, inplace=True)

        self.non_gaussian_scalars.append("iva_dagar_n")

    def _iva_vardtillfallen_n(self):
        self.df_processed["iva_vardtillfallen_n"] = self.df_raw["iva_vardtillfallen_n"]

        if self.impute:
            self.df_processed["iva_vardtillfallen_n"].fillna(0, inplace=True)

        self.non_gaussian_scalars.append("iva_vardtillfallen_n")

    def _waran_beh_vid_ank(self):
        self.df_processed["waran_beh_vid_ank"] = self.df_raw["waran_beh_vid_ank"]

        self.__encode_yes_no("waran_beh_vid_ank")

    def _noak_vid_ankomst(self):
        self.df_processed["noak_vid_ankomst"] = self.df_raw["noak_vid_ankomst"]

        self.__encode_yes_no("noak_vid_ankomst")

    def _vk_all(self, column: str):
        self.df_processed[column] = self.df_raw[column]

        self.__encode_yes_no(column)

    def _ofi(self):
        self.df_processed["ofi"] = self.df_raw["ofi"]
        self.df_processed["ofi_raw"] = self.df_raw["ofi"]

        self.__encode_yes_no("ofi")
