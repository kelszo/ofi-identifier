import json
from typing import List, Tuple, Union

import numpy as np
import pandas as pd

from ofiidentifier.utils.logging import logging

with open("/data/processed/category_columns.json") as f:
    category_columns = json.load(f)


class OFIDataset:
    def __init__(self, path: str, features: List[str] = [], features_raw: List[str] = []):
        self._df = pd.read_csv(path, index_col="id")
        self.features = []

        self.features.extend(features_raw)

        for column in self._df.columns:
            for feature in features:
                feature = feature.replace("*", "")
                if column.startswith(feature):
                    self.features.append(column)

        self.features = list(set(self.features))
        self.features.sort()

        if "fold" in self._df.columns:
            self.n_folds = self._df["fold"].nunique()

        self.cat_idxs: List[int] = []
        self.cat_dims: List[int] = []
        self.feat_type: List[str] = []
        self.categories: List[str] = []
        self.numerical: List[str] = []

        for (i, feature) in enumerate(self.features):
            if category_columns.get(feature) is not None:
                self.feat_type.append("Categorical")
                self.categories.append(feature)
                self.cat_idxs.append(i)
                self.cat_dims.append(category_columns[feature])
                self._df[feature] = self._df[feature].astype("int")
                self._df[feature] = self._df[feature].astype("category")
            else:
                self.feat_type.append("Numerical")
                self.numerical.append(feature)

        _, counts = np.unique(self._df["ofi"], return_counts=True)
        self.scale_pos_weight = counts[0] / counts[1]

        logging.debug(f"Parsed following columns: {', '.join(self.features)}")
        logging.debug(f"Using scale pos weight: {self.scale_pos_weight}")

    def df(self) -> pd.DataFrame:
        return self._df[self.features + ["ofi"]]

    def train_ds(self, fold: Union[str, int] = None) -> Tuple[np.ndarray, np.ndarray]:
        if fold is not None:
            df_train = self._df[self._df.fold != fold]

            X_train = df_train[self.features].to_numpy()
            y_train = df_train["ofi"].to_numpy()

            return (X_train, y_train)
        else:
            X_train = self._df[self.features].to_numpy()
            y_train = self._df["ofi"].to_numpy()

            return (X_train, y_train)

    def val_ds(self, fold: Union[str, int]) -> Tuple[np.ndarray, np.ndarray]:
        if fold is None:
            raise Exception("No fold")

        df_val = self._df[self._df.fold == fold]

        X_val = df_val[self.features].to_numpy()
        y_val = df_val["ofi"].to_numpy()

        return (X_val, y_val)

    def train_df(self, fold: Union[str, int] = None) -> Tuple[pd.DataFrame, pd.Series]:
        if fold is not None:
            df_train = self._df[self._df.fold != fold]

            X_train = df_train[self.features]
            y_train = df_train["ofi"]

            return (X_train, y_train)
        else:
            X_train = self._df[self.features]
            y_train = self._df["ofi"]

            return (X_train, y_train)

    def val_df(self, fold: Union[str, int]) -> Tuple[pd.DataFrame, pd.Series]:
        if fold is None:
            raise Exception("No fold")

        df_val = self._df[self._df.fold == fold]

        X_val = df_val[self.features]
        y_val = df_val["ofi"]

        return (X_val, y_val)

    def test_ds(self) -> Tuple[np.ndarray, np.ndarray]:
        X_test = self._df[self.features].to_numpy()
        y_test = self._df["ofi"].to_numpy()

        return (X_test, y_test)

    def test_df(self) -> Tuple[pd.DataFrame, pd.Series]:
        X_test = self._df[self.features]
        y_test = self._df["ofi"]

        return (X_test, y_test)
