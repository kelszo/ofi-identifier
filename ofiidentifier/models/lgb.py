import multiprocessing
from typing import Union
import pickle

import numpy as np
import pandas as pd
from hpsklearn import HyperoptEstimator, lightgbm_classification
from hyperopt import tpe
from lightgbm import Booster, LGBMClassifier

from ofiidentifier.data.ofi_dataset import OFIDataset
from ofiidentifier.utils.loss_funcs import roc_loss


class OFILGB:
    def __init__(self, seed: int):
        self.model: LGBMClassifier = None
        self.seed = seed

    def hyper_opt(self, dataset: OFIDataset, max_evals: int, scale_pos_weight: float) -> dict:
        X, y = dataset.train_ds()

        self.estim = HyperoptEstimator(
            classifier=lightgbm_classification("clf", scale_pos_weight=scale_pos_weight),
            algo=tpe.suggest,
            trial_timeout=3600,
            max_evals=max_evals,
            loss_fn=roc_loss,
            seed=self.seed,
            refit=False,
            verbose=False,
            # continuous_loss_fn=True,
            n_jobs=max(multiprocessing.cpu_count(), 8),
        )

        self.estim.fit(X, y, n_folds=5, cv_shuffle=True)
        self.model = self.estim.best_model()["learner"]

        X, y = dataset.train_df()
        self.model.fit(X, y)

        return self.model.get_params()

    def fit(self, X: Union[np.ndarray, pd.DataFrame], y: Union[np.ndarray, pd.Series]):
        self.classes_, y = np.unique(y, return_inverse=True)

        self.model.fit(X, y)

    def save(self, path: str):
        self.__trained()

        pickle.dump(self.model, open(path, "wb"))

        # self.model.booster_.save_model(path)

    def load_from_params(self, params: dict):
        self.model = LGBMClassifier(**params)

    def load(self, path: str):
        self.model = pickle.load(open(path, "rb"))

    def predict_proba(self, data: np.ndarray) -> np.ndarray:
        self.__trained()

        return self.model.predict_proba(data)[:, 1]

    def predict(self, data: np.ndarray) -> np.ndarray:
        self.__trained()

        return self.model.predict(data)

    def __trained(self):
        if self.model is None:
            raise Exception("Model is not trained")
