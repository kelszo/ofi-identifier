import multiprocessing

import numpy as np
from hpsklearn import HyperoptEstimator, xgboost_classification
from hyperopt import tpe
from xgboost import XGBClassifier

from ofiidentifier.utils.loss_funcs import roc_loss


class OFIXGB:
    def __init__(self, seed: int):
        self.model: XGBClassifier = None
        self.seed = seed

    def hyper_opt(self, X: np.ndarray, y: np.ndarray, max_evals: int, scale_pos_weight: float) -> dict:
        self.estim = HyperoptEstimator(
            classifier=xgboost_classification("clf", scale_pos_weight=scale_pos_weight),
            algo=tpe.suggest,
            trial_timeout=3600,
            max_evals=max_evals,
            loss_fn=roc_loss,
            seed=self.seed,
            refit=True,
            verbose=False,
            # continuous_loss_fn=True,
            n_jobs=max(multiprocessing.cpu_count(), 8),
        )

        self.estim.fit(X, y, n_folds=5, cv_shuffle=True, random_state=self.seed)
        self.model = self.estim.best_model()["learner"]

        return self.model.get_params()

    def fit(self, X: np.ndarray, y: np.ndarray):
        self.model.fit(X, y)

    def save(self, path, features):
        self.__trained()

        self.model.get_booster().feature_names = features

        self.model.save_model(path)

    def load(self, path):
        self.model = XGBClassifier()

        self.model.load_model(path)

    def load_from_params(self, params):
        self.model = XGBClassifier(**params)

    def predict_proba(self, data):
        self.__trained()

        return self.model.predict_proba(data)[:, 1]

    def predict(self, data):
        self.__trained()

        return self.model.predict(data)

    def __trained(self):
        if self.model is None:
            raise Exception("Model is not trained")
