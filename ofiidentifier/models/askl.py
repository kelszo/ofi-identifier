import pickle

import numpy as np

from autosklearn.experimental.askl2 import AutoSklearn2Classifier
from autosklearn.metrics import roc_auc


class OFIASKL:
    def __init__(self, seed: int):
        self.model: AutoSklearn2Classifier = None
        self.seed = seed

    def hyper_opt(self, X, y, feat_type, time):
        self.model = AutoSklearn2Classifier(
            metric=roc_auc,
            time_left_for_this_task=time,
            tmp_folder="/tmp/autosklearn_resampling_example_tmp",
            disable_evaluator_output=False,
            memory_limit=None,
            n_jobs=8,
            seed=self.seed,
        )

        self.model.fit(X, y, feat_type=feat_type, dataset_name="OFI")

    def refit(self, X: np.ndarray, y: np.ndarray):
        self.model.refit(X, y)

    def save(self, path: str):
        self.__trained()

        pickle.dump(self.model, open(path, "wb"))

    def load(self, path: str):
        self.model = pickle.load(open(path, "rb"))

    def predict_proba(self, data: np.ndarray) -> np.ndarray:
        self.__trained()

        return self.model.predict_proba(data)[:, 1]

    def predict(self, data: np.ndarray) -> np.ndarray:
        self.__trained()

        return self.model.predict(data)

    def __trained(self):
        if self.model is None:
            raise Exception("Model is not trained")
