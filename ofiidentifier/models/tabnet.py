from typing import Union

import numpy as np
import optuna
import torch
from pytorch_tabnet.augmentations import ClassificationSMOTE
from pytorch_tabnet.pretraining import TabNetPretrainer
from pytorch_tabnet.tab_model import TabNetClassifier

from ofiidentifier.data.ofi_dataset import OFIDataset


class OFITabNet:
    def __init__(self, seed: int):
        self.model: TabNetClassifier = None
        self.unsupervised_model: TabNetPretrainer = None
        self.seed = seed

    def hyper_opt(self, dataset: OFIDataset, n_trials: int = None) -> dict:
        def Objective(trial: optuna.Trial):
            n_da = trial.suggest_categorical("n_da", [8, 16, 32, 64, 128])
            n_d = n_da
            n_a = n_da
            n_steps = trial.suggest_categorical("n_steps", [1, 2, 5])
            gamma = trial.suggest_float("gamma", 1.0, 2.0, step=0.2)
            n_independent = trial.suggest_categorical("n_independent", [1, 2, 5])
            n_shared = trial.suggest_categorical("n_shared", [1, 2, 5])
            momentum = trial.suggest_float("momentum", 0.01, 0.4)
            lambda_sparse = trial.suggest_float("lambda_sparse", 1e-7, 1e-3)
            mask_type = "sparsemax"  # mask_type = trial.suggest_categorical("mask_type", ["entmax", "sparsemax"])

            n_shared_decoder = 1  # n_shared_decoder = trial.suggest_int("n_shared_decoder", 1, 5, step=1)
            n_indep_decoder = 1  # n_indep_decoder = trial.suggest_int("n_indep_decoder", 1, 5, step=1)

            batch_size = 1024  # batch_size = trial.suggest_int("batch_size", 256, 4096)
            virtual_batch_size = trial.suggest_categorical("virtual_batch_size", [32, 64, 128, 256, 512, 1024])

            unsupervised_epochs = 1000
            # unsupervised_epochs = trial.suggest_int("unsupervised_epochs", 100, 1000, step=100)

            clip_value = None  # clip_value = trial.suggest_categorical("clip_value", [0.5, 1, 2, 5, None])
            optimizer_fn = torch.optim.AdamW
            weight_decay = trial.suggest_float("weight_decay", 1e-5, 0.1)

            if weight_decay != 0:
                optimizer_params = {"lr": 2e-2, "weight_decay": weight_decay}
            else:
                optimizer_params = {"lr": 2e-2}

            scheduler_fn = torch.optim.lr_scheduler.ReduceLROnPlateau
            scheduler_params = {
                "mode": "min",
                "patience": 5,
                "min_lr": 1e-5,
                "factor": 0.5,
            }

            augmentation_p = trial.suggest_float("augmentation_p", 0, 0.4, step=0.1)
            if augmentation_p != 0:
                augmentation = ClassificationSMOTE(p=augmentation_p)
            else:
                augmentation = None

            pretraining_ratio = trial.suggest_categorical("pretraining_ratio", [0.1, 0.25, 0.5, 0.75, 0.9])

            cv_score_array = []
            for fold in range(dataset.n_folds):
                X_train, y_train = dataset.train_ds(fold)
                X_valid, y_valid = dataset.val_ds(fold)

                unsupervised_model = TabNetPretrainer(
                    n_d=n_d,
                    n_a=n_a,
                    n_steps=n_steps,
                    gamma=gamma,
                    cat_idxs=dataset.cat_idxs,
                    cat_dims=dataset.cat_dims,
                    n_independent=n_independent,
                    n_shared=n_shared,
                    seed=self.seed,
                    momentum=momentum,
                    lambda_sparse=lambda_sparse,
                    clip_value=clip_value,
                    optimizer_fn=optimizer_fn,
                    optimizer_params=optimizer_params,
                    mask_type=mask_type,
                    n_shared_decoder=n_shared_decoder,
                    n_indep_decoder=n_indep_decoder,
                    verbose=0,
                )

                unsupervised_model.fit(
                    X_train=X_train,
                    eval_set=[X_valid],
                    pretraining_ratio=pretraining_ratio,
                    max_epochs=unsupervised_epochs,
                    patience=50,
                    batch_size=batch_size,
                    virtual_batch_size=virtual_batch_size,
                    num_workers=8,
                    drop_last=False,
                )

                model = TabNetClassifier(
                    n_d=n_d,
                    n_a=n_a,
                    n_steps=n_steps,
                    gamma=gamma,
                    cat_idxs=dataset.cat_idxs,
                    cat_dims=dataset.cat_dims,
                    n_independent=n_independent,
                    n_shared=n_shared,
                    seed=self.seed,
                    momentum=momentum,
                    lambda_sparse=lambda_sparse,
                    clip_value=clip_value,
                    optimizer_fn=optimizer_fn,
                    optimizer_params=optimizer_params,
                    scheduler_fn=scheduler_fn,
                    scheduler_params=scheduler_params,
                    mask_type=mask_type,
                    verbose=0,
                )

                model.fit(
                    X_train=X_train,
                    y_train=y_train,
                    eval_set=[(X_train, y_train), (X_valid, y_valid)],
                    eval_name=["train", "valid"],
                    eval_metric=["auc"],
                    max_epochs=100,
                    patience=25,
                    weights=1,
                    batch_size=batch_size,
                    virtual_batch_size=virtual_batch_size,
                    num_workers=8,
                    drop_last=False,
                    from_unsupervised=unsupervised_model,
                    augmentations=augmentation,
                )

                cv_score_array.append(model.best_cost)

            avg = np.mean(cv_score_array)
            return avg

        study = optuna.create_study(direction="maximize", study_name="OFI TabNet optimization")
        study.optimize(Objective, gc_after_trial=True, n_trials=n_trials)

        return study.best_params

    def save(self, path: str):
        self.__trained()

        self.model.save_model(path)

    def load(self, path: str):
        self.model = TabNetClassifier()

        self.model.load_model(path)

    def train_from_params(self, dataset: OFIDataset, params: dict, fold: Union[str, int]):
        X_train, y_train = dataset.train_ds(fold)
        X_valid, y_valid = dataset.val_ds(fold)

        n_da = params["n_da"]
        n_d = n_da
        n_a = n_da
        n_steps = params["n_steps"]
        gamma = params["gamma"]
        n_independent = params["n_independent"]
        n_shared = params["n_shared"]
        momentum = params["momentum"]
        lambda_sparse = params["lambda_sparse"]
        mask_type = "sparsemax"  # mask_type = params["mask_type"]

        n_shared_decoder = 1  # n_shared_decoder = params["n_shared_decoder"]
        n_indep_decoder = 1  # n_indep_decoder = params["n_indep_decoder"]

        batch_size = 1024  # batch_size = params["batch_size"]
        virtual_batch_size = params["virtual_batch_size"]

        unsupervised_epochs = 1000
        # unsupervised_epochs = params["unsupervised_epochs"]

        clip_value = None  # clip_value = params["clip_value"]

        optimizer_fn = torch.optim.AdamW
        weight_decay = params["weight_decay"]
        if weight_decay != 0:
            optimizer_params = {"lr": 2e-2, "weight_decay": weight_decay}
        else:
            optimizer_params = {"lr": 2e-2}

        scheduler_fn = torch.optim.lr_scheduler.ReduceLROnPlateau
        scheduler_params = {"mode": "min", "patience": 5, "min_lr": 1e-5, "factor": 0.5}

        augmentation_p = params["augmentation_p"]
        if augmentation_p != 0:
            augmentation = ClassificationSMOTE(p=augmentation_p)
        else:
            augmentation = None

        pretraining_ratio = params["pretraining_ratio"]

        self.unsupervised_model = TabNetPretrainer(
            n_d=n_d,
            n_a=n_a,
            n_steps=n_steps,
            gamma=gamma,
            cat_idxs=dataset.cat_idxs,
            cat_dims=dataset.cat_dims,
            n_independent=n_independent,
            n_shared=n_shared,
            seed=self.seed,
            momentum=momentum,
            lambda_sparse=lambda_sparse,
            clip_value=clip_value,
            optimizer_fn=optimizer_fn,
            optimizer_params=optimizer_params,
            mask_type=mask_type,
            n_shared_decoder=n_shared_decoder,
            n_indep_decoder=n_indep_decoder,
            verbose=0,
        )

        self.unsupervised_model.fit(
            X_train=X_train,
            eval_set=[X_valid],
            pretraining_ratio=pretraining_ratio,
            max_epochs=unsupervised_epochs,
            patience=50,
            batch_size=batch_size,
            virtual_batch_size=virtual_batch_size,
            num_workers=8,
            drop_last=False,
        )

        self.model = TabNetClassifier(
            n_d=n_d,
            n_a=n_a,
            n_steps=n_steps,
            gamma=gamma,
            cat_idxs=dataset.cat_idxs,
            cat_dims=dataset.cat_dims,
            n_independent=n_independent,
            n_shared=n_shared,
            seed=self.seed,
            momentum=momentum,
            lambda_sparse=lambda_sparse,
            clip_value=clip_value,
            optimizer_fn=optimizer_fn,
            optimizer_params=optimizer_params,
            scheduler_fn=scheduler_fn,
            scheduler_params=scheduler_params,
            mask_type=mask_type,
            verbose=0,
        )

        self.model.fit(
            X_train=X_train,
            y_train=y_train,
            eval_set=[(X_train, y_train), (X_valid, y_valid)],
            eval_name=["train", "valid"],
            eval_metric=["auc"],
            max_epochs=100,
            patience=25,
            weights=1,
            batch_size=batch_size,
            virtual_batch_size=virtual_batch_size,
            num_workers=8,
            drop_last=False,
            from_unsupervised=self.unsupervised_model,
            augmentations=augmentation,
        )

    def predict_proba(self, data: np.ndarray) -> np.ndarray:
        self.__trained()

        return self.model.predict_proba(data)[:, 1]

    def predict(self, data: np.ndarray) -> np.ndarray:
        self.__trained()

        return self.model.predict(data)

    def __trained(self):
        if self.model is None:
            raise Exception("Model is not trained")
