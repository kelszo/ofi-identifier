from typing import Tuple

import numpy as np
from catboost import CatBoostClassifier
import optuna
from sklearn.metrics import roc_auc_score

from ofiidentifier.data.ofi_dataset import OFIDataset


class OFICAT:
    def __init__(self, seed: int):
        self.model: CatBoostClassifier = None
        self.seed = seed

    def hyper_opt(self, dataset: OFIDataset, timeout: int) -> dict:
        def Objective(trial: optuna.Trial):
            param = {
                "objective": trial.suggest_categorical("objective", ["Logloss", "CrossEntropy"]),
                "colsample_bylevel": trial.suggest_float("colsample_bylevel", 0.01, 0.1, log=True),
                "depth": trial.suggest_int("depth", 1, 12),
                "boosting_type": trial.suggest_categorical("boosting_type", ["Ordered", "Plain"]),
                "bootstrap_type": trial.suggest_categorical("bootstrap_type", ["Bayesian", "Bernoulli", "MVS"]),
                "eval_metric": trial.suggest_categorical("eval_metric", ["AUC"]),
                "random_state": trial.suggest_categorical("random_state", [self.seed]),
                "cat_features": dataset.cat_idxs,
                "l2_leaf_reg": trial.suggest_float("l2_leaf_reg", 1.0, 5.5, step=0.5),
                "train_dir": "/tmp",
            }

            if param["objective"] == "Logloss":
                param["scale_pos_weight"] = trial.suggest_categorical("scale_pos_weight", [dataset.scale_pos_weight])

            if param["bootstrap_type"] == "Bayesian":
                param["bagging_temperature"] = trial.suggest_float("bagging_temperature", 0, 10)
            elif param["bootstrap_type"] == "Bernoulli":
                param["subsample"] = trial.suggest_float("subsample", 0.1, 1, log=True)

            probas = []
            targets = []
            for fold in range(dataset.n_folds):
                X_train, y_train = dataset.train_df(fold)
                X_valid, y_valid = dataset.val_df(fold)

                model = CatBoostClassifier(**param)

                model.fit(
                    X_train,
                    y_train,
                    eval_set=[(X_valid, y_valid)],
                    use_best_model=True,
                    early_stopping_rounds=100,
                    verbose=0,
                )

                fold_probas = model.predict_proba(X_valid)[:, 1]

                targets.extend(y_valid)
                probas.extend(fold_probas)

            score = roc_auc_score(targets, probas)

            return score

        study = optuna.create_study(
            direction="maximize",
            study_name="CatBoost optimization",
        )
        study.optimize(Objective, gc_after_trial=True, timeout=timeout)

        best_params = study.best_params
        best_params["cat_features"] = dataset.cat_idxs
        best_params["train_dir"] = "/tmp"

        # retrain
        self.load_from_params(best_params)
        X_train, y_train = dataset.train_df()
        self.model.fit(X_train, y_train, verbose=0, use_best_model=True)

        return best_params

    def fit(self, X: np.ndarray, y: np.ndarray, eval_set: Tuple[np.ndarray, np.ndarray] = None):
        self.model.fit(
            X,
            y,
            eval_set=[eval_set] if eval_set is not None else None,
            verbose=0,
            use_best_model=True,
            early_stopping_rounds=100,
        )

    def save(self, path: str):
        self.__trained()

        self.model.save_model(path)

    def load(self, path: str):
        self.model = CatBoostClassifier()

        self.model.load_model(path)

    def load_from_params(self, params: dict):
        params["train_dir"] = "/tmp"

        self.model = CatBoostClassifier(**params)

    def predict_proba(self, data: np.ndarray) -> np.ndarray:
        self.__trained()

        return self.model.predict_proba(data)[:, 1]

    def predict(self, data: np.ndarray) -> np.ndarray:
        self.__trained()

        return self.model.predict(data)

    def __trained(self):
        if self.model is None:
            raise Exception("Model is not trained")
