from typing import Tuple
import multiprocessing
import pickle

import numpy as np
from autoPyTorch.api.tabular_classification import TabularClassificationTask
from autoPyTorch.utils.hyperparameter_search_space_update import HyperparameterSearchSpaceUpdates
from sklearn.metrics import roc_auc_score

from ofiidentifier.data.ofi_dataset import OFIDataset


class OFIMLP:
    def __init__(self, seed: int):
        self.model: TabularClassificationTask = None
        self.seed = seed

    def hyper_opt(self, dataset: OFIDataset, timeout: int) -> dict:
        X_train, y_train = dataset.train_df()

        self.model = TabularClassificationTask(seed=2022)

        self.model.search(
            X_train=X_train,
            y_train=y_train,
            optimize_metric="roc_auc",
            total_walltime_limit=timeout,
            #func_eval_time_limit_secs=timeout // 10,
            memory_limit=None,
            enable_traditional_pipeline=False,
            feat_types=[x.lower() for x in dataset.feat_type],
        )

    def refit(self, X: np.ndarray, y: np.ndarray, X_test: np.ndarray = None, y_test: np.ndarray = None):
        dataset = self.model.get_dataset(X, y, X_test=X_test, y_test=y_test)

        self.model.refit(dataset)

    def predict_proba(self, data: np.ndarray) -> np.ndarray:
        self.__trained()

        return self.model.predict_proba(data)[:, 1]

    def predict(self, data: np.ndarray) -> np.ndarray:
        self.__trained()

        return self.model.predict(data)

    def save(self, path):
        with open(path, 'wb') as f:
            pickle.dump(self.model, f, protocol=pickle.HIGHEST_PROTOCOL)

    def load(self, path):
        with open(path, 'rb') as f:
            self.model = pickle.load(f)

    def __trained(self):
        if self.model is None:
            raise Exception("Model is not trained")
