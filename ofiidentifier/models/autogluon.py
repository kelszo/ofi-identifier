import uuid
from typing import Union

import numpy as np
import pandas as pd

from autogluon.tabular import TabularDataset, TabularPredictor

from ofiidentifier.data.ofi_dataset import OFIDataset


class OFIAG:
    def __init__(self, seed: int):
        self.model: TabularPredictor = None
        self.seed = seed

    def hyper_opt(self, dataset: OFIDataset, time: int, save_path: str = None):
        train_data = TabularDataset(dataset.df())

        self.model = TabularPredictor(
            label="ofi",
            path=f"/tmp/ag-{uuid.uuid4()}" if save_path is None else save_path,
            eval_metric="roc_auc",
            problem_type="binary",
            sample_weight="auto_weight",
        )

        hyperparameters = {
            # "NN_TORCH": {},
            "GBM": [
                {"extra_trees": True, "ag_args": {"name_suffix": "XT"}},
                {},
                "GBMLarge",
                {
                    "boosting_type": "dart",
                    "class_weight": None,
                    "colsample_bytree": 0.7789575510130504,
                    "importance_type": "split",
                    "learning_rate": 0.009853572626710608,
                    "max_depth": 2,
                    "min_child_samples": 20,
                    "min_child_weight": 3,
                    "min_split_gain": 0.0,
                    "n_estimators": 2000,
                    "n_jobs": 8,
                    "num_leaves": 85,
                    "objective": "binary",
                    "random_state": None,
                    "reg_alpha": 0.9860750442245387,
                    "reg_lambda": 2.6711193274538068,
                    "silent": "warn",
                    "subsample": 0.7129940013643866,
                    "subsample_for_bin": 200000,
                    "subsample_freq": 0,
                    "max_delta_step": 0,
                    "scale_pos_weight": 33.99261992619926,
                    "seed": 2022,
                    "ag_args": {"name_suffix": "CLGB"},
                },
            ],
            "CAT": {
                "objective": "CrossEntropy",
                "colsample_bylevel": 0.06612012311841957,
                "depth": 9,
                "boosting_type": "Ordered",
                "bootstrap_type": "MVS",
                "eval_metric": "AUC",
                "random_state": 2022,
                "l2_leaf_reg": 4.0,
            },
            "XGB": {
                "objective": "binary:logistic",
                "use_label_encoder": False,
                "base_score": 0.5,
                "booster": "gbtree",
                "callbacks": None,
                "colsample_bylevel": 0.5025579968087006,
                "colsample_bynode": 1,
                "colsample_bytree": 0.5621782534857103,
                "early_stopping_rounds": None,
                "enable_categorical": False,
                "eval_metric": None,
                "gamma": 0.03088956662701315,
                "gpu_id": -1,
                "grow_policy": "depthwise",
                "importance_type": None,
                "interaction_constraints": "",
                "learning_rate": 0.0004687645278617215,
                "max_bin": 256,
                "max_cat_to_onehot": 4,
                "max_delta_step": 0,
                "max_depth": 2,
                "max_leaves": 0,
                "min_child_weight": 23,
                "monotone_constraints": "()",
                "n_estimators": 3200,
                "n_jobs": 8,
                "num_parallel_tree": 1,
                "predictor": "auto",
                "random_state": 4,
                "reg_alpha": 0.016517496411859248,
                "reg_lambda": 1.6896746583020843,
                "sampling_method": "uniform",
                "scale_pos_weight": 33.99261992619926,
                "subsample": 0.998729915800979,
                "tree_method": "exact",
                "validate_parameters": 1,
                "verbosity": None,
                "seed": 2022,
            },
            # "FASTAI": {},
            "RF": [
                {"criterion": "gini", "ag_args": {"name_suffix": "Gini", "problem_types": ["binary", "multiclass"]}},
                {"criterion": "entropy", "ag_args": {"name_suffix": "Entr", "problem_types": ["binary", "multiclass"]}},
                {"criterion": "squared_error", "ag_args": {"name_suffix": "MSE", "problem_types": ["regression"]}},
            ],
            "XT": [
                {"criterion": "gini", "ag_args": {"name_suffix": "Gini", "problem_types": ["binary", "multiclass"]}},
                {"criterion": "entropy", "ag_args": {"name_suffix": "Entr", "problem_types": ["binary", "multiclass"]}},
                {"criterion": "squared_error", "ag_args": {"name_suffix": "MSE", "problem_types": ["regression"]}},
            ],
            "KNN": [
                {"weights": "uniform", "ag_args": {"name_suffix": "Unif"}},
                {"weights": "distance", "ag_args": {"name_suffix": "Dist"}},
            ],
        }

        self.model.fit(
            train_data,
            # ag_args_fit={'num_gpus': 1},
            presets="best_quality",
            feature_prune_kwargs={},
            time_limit=time,
            keep_only_best=False,
            save_space=False,
            hyperparameters=hyperparameters,
            excluded_model_types=["FASTAI, NN"],
        )

    def save(self):
        self.model.save()

    def load(self, path: str):
        self.model = TabularPredictor.load(path)

    def predict_proba(self, data: Union[np.ndarray, pd.DataFrame]) -> np.ndarray:
        self.__trained()

        return self.model.predict_proba(data, as_pandas=False, as_multiclass=False)

    def predict(self, data: Union[np.ndarray, pd.DataFrame]) -> np.ndarray:
        self.__trained()

        return self.model.predict(data, as_pandas=False)

    def __trained(self):
        if self.model is None:
            raise Exception("Model is not trained")
