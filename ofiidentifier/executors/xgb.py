#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import os
import time
from argparse import ArgumentParser
from typing import List

import numpy as np
from pytorch_lightning import seed_everything
from scipy.stats import mode
from sklearn.metrics import confusion_matrix, f1_score, roc_auc_score
from tqdm import tqdm

from ofiidentifier.data.ofi_dataset import OFIDataset
from ofiidentifier.models.xgb import OFIXGB
from ofiidentifier.utils.argparse_types import dir_path, file_path, path
from ofiidentifier.utils.logging import logging


def process_codes(ais: List[str], icd: List[str], pac: List[str]):
    return [f"ais_{x}" for x in ais] + [f"icd_{x}" for x in icd] + [f"pac_{x}" for x in pac]


features = [
    "pt_age_yrs",
    "pt_Gender",
    "inj_dominant",
    "inj_mechanism",
    "inj_intention",
    "pt_asa_preinjury",
    "pre_card_arrest",
    "pre_gcs_sum",
    "ed_gcs_sum",
    "pre_sbp_value",
    "ed_sbp_value",
    "pre_rr_value",
    "ed_rr_value",
    "ed_be_art",
    "ed_inr",
    "hosp_vent_days",
    "hosp_los_days",
    "hosp_dischg_dest",
    "res_gos_dischg",
    "res_survival",
    # "TraumaAlarmCriteria",
    "TraumaAlarmAtHospital",
    "AlarmRePrioritised",
    "ISS",
    "dt_alarm_hosp",
    "pre_provided",
    "pre_intubated",
    # "pre_intub_type",
    "ed_intubated",
    # "ed_intub_type",
    "pre_transport",
    "ed_emerg_proc",
    "ed_emerg_proc_other",
    "ed_tta",
    "host_transfered",
    "host_care_level",
    "dt_alarm_scene",
    "dt_ed_first_ct",
    "dt_ed_emerg_proc",
    "NumberOfActions",
    "NumberOfInjuries",
    "filter_*",
    # "pac_*",
    # "icd_*",
    # "ais_*",
    # "Tr_Niva",
    # "iva_dagar_n",
    # "iva_vardtillfallen_n",
    # "waran_beh_vid_ank",
    # "noak_vid_ankomst",
    # "VK_hlr_thorak",
    # "VK_sap_less90",
    # "VK_leverskada",
    # "VK_gcs_less9_ej_intubTE",
    # "VK_mjaltskada",
    # "VK_mer_30min_DT",
    # "VK_mass_transf",
    # "VK_mer_60min_interv",
    # "VK_iss_15_ej_iva",
    # "VK_ej_trombrof_TBI_72h",
    # "VK_iss_15_ej_TE",
    # "ofi",
]

# Good codes with diff
ais = [310402.1, 910400.1, 450203.3, 442205.3, 210202.1, 856151.2, 510202.1]

icd = ["S37.00", "S22.40", "S06.0", "S27.00", "T14.0", "S32.80", "S13.4"]

pac = ["NAG79", "QDB05", "QAB00", "TNG32"]

features_raw = process_codes(ais, icd, pac)

best_hyper_opts = {
    "objective": "binary:logistic",
    "use_label_encoder": False,
    "base_score": 0.5,
    "booster": "gbtree",
    "callbacks": None,
    "colsample_bylevel": 0.5025579968087006,
    "colsample_bynode": 1,
    "colsample_bytree": 0.5621782534857103,
    "early_stopping_rounds": None,
    "enable_categorical": False,
    "eval_metric": None,
    "gamma": 0.03088956662701315,
    "gpu_id": -1,
    "grow_policy": "depthwise",
    "importance_type": None,
    "interaction_constraints": "",
    "learning_rate": 0.0004687645278617215,
    "max_bin": 256,
    "max_cat_to_onehot": 4,
    "max_delta_step": 0,
    "max_depth": 2,
    "max_leaves": 0,
    "min_child_weight": 23,
    "monotone_constraints": "()",
    "n_estimators": 3200,
    "n_jobs": 8,
    "num_parallel_tree": 1,
    "predictor": "auto",
    "random_state": 4,
    "reg_alpha": 0.016517496411859248,
    "reg_lambda": 1.6896746583020843,
    "sampling_method": "uniform",
    "scale_pos_weight": 33.99261992619926,
    "subsample": 0.998729915800979,
    "tree_method": "exact",
    "validate_parameters": 1,
    "verbosity": None,
    "seed": 2022,
}

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("experiment_name", type=str)
    parser.add_argument(
        "--ofi-dataset-path", default="/data/processed/ofi_train.onehotencoded.imputed.standardised.csv", type=file_path
    )
    parser.add_argument(
        "--test-dataset-path",
        default="/data/processed/ofi_test.onehotencoded.imputed.standardised.csv",
        type=file_path,
    )
    parser.add_argument("--cv", dest="cv", action="store_true", default=False)
    parser.add_argument("--hyper-opt", dest="hyper_opt", action="store_true", default=False)
    parser.add_argument("--load-model", type=path, default=None)
    parser.add_argument("--max-evals", type=int, default=150)
    parser.add_argument("--out-path", type=dir_path, default="/app/out")
    parser.add_argument("--save", dest="save", action="store_true", default=False)
    parser.add_argument("--seed", type=int, default=2022)
    parser.add_argument("--test", dest="test", action="store_true", default=False)

    P = parser.parse_args()
    P.experiment_name = P.experiment_name.replace(" ", "_")

    seed_everything(P.seed, workers=True)

    logging.info(f"STARING: XGB. EXPERIMENT NAME: {P.experiment_name}")
    start_time_string = time.strftime("%m%d-%H%M")

    dataset = OFIDataset(P.ofi_dataset_path, features=features, features_raw=features_raw)

    experiment_results = {
        "model": "XGB",
        "start_time": start_time_string,
        "experiment_name": P.experiment_name,
        "columns": dataset.features,
    }

    ofiXGB = OFIXGB(seed=P.seed)
    models = None

    if P.hyper_opt:
        logging.info(f"STARTING: HYPER OPTIMISATION")
        X, y = dataset.train_ds()
        best_hyper_opts = ofiXGB.hyper_opt(X, y, max_evals=P.max_evals, scale_pos_weight=dataset.scale_pos_weight)
        print("Best hyper opts:", best_hyper_opts)

        best_hyper_opts["seed"] = int(best_hyper_opts["seed"])
        experiment_results["best_hyper_opts"] = best_hyper_opts

        if P.save:
            model_path = P.out_path / "models" / f"{start_time_string}-xgb-complete.json"
            ofiXGB.save(model_path, dataset.features)

        logging.info(f"FINISHED: HYPER OPTIMISATION")
    elif P.load_model is not None:
        if os.path.isdir(P.load_model):
            models = []
            model_names = os.listdir(P.load_model)
            model_names.sort()

            for model_name in model_names:
                ofiXGB = OFIXGB(seed=P.seed)
                ofiXGB.load(P.load_model / model_name)
                models.append(ofiXGB)
        else:
            ofiXGB.load(P.load_model)

        dataset.features = ofiXGB.model.get_booster().feature_names
    else:
        ofiXGB.load_from_params(best_hyper_opts)

    if P.cv:
        logging.info(f"STARTING: CROSS VALIDATION")

        cv_models = []
        cv_targets = []
        cv_preds = []
        cv_probas = []

        for fold in tqdm(range(dataset.n_folds)):
            X_train, y_train = dataset.train_ds(fold)

            if models is not None:
                ofiXGB = models[fold]
            else:
                ofiXGB = OFIXGB(seed=P.seed)
                ofiXGB.load_from_params(best_hyper_opts)
                ofiXGB.fit(X_train, y_train)

            X_val, y_val = dataset.val_ds(fold)

            fold_preds = ofiXGB.predict(X_val)
            fold_proba = ofiXGB.predict_proba(X_val)

            cv_targets = np.append(cv_targets, y_val)
            cv_preds = np.append(cv_preds, fold_preds)
            cv_probas = np.append(cv_probas, fold_proba)

            cv_models.append(ofiXGB)

            if P.save:
                model_dir = P.out_path / "models" / f"{start_time_string}-xgb"

                model_dir.mkdir(parents=True, exist_ok=True)

                ofiXGB.save(model_dir / f"{fold}.json", dataset.features)

        models = cv_models

        cv_auc = roc_auc_score(cv_targets, cv_probas)
        cv_f1 = f1_score(cv_targets, cv_preds)
        cv_c_matrix = confusion_matrix(cv_targets, cv_preds)

        print(f"[CV] AUC SCORE: {cv_auc}")
        print(f"[CV] f1 SCORE: {cv_f1}")
        print("[CV]", cv_c_matrix)

        experiment_results["cv"] = {
            "auc": cv_auc,
            "f1": cv_f1,
            "confusion_matrix": cv_c_matrix.tolist(),
            "probas": cv_probas.tolist(),
            "targets": cv_targets.tolist(),
        }

        logging.info("DONE: CROSS VALIDATION")

    if P.test:
        logging.info(f"STARTING: TESTING")

        test_dataset = OFIDataset(P.test_dataset_path, features_raw=dataset.features)
        X_test, y_test = test_dataset.test_ds()

        if models is not None:
            test_preds = []
            test_probas = []
            for ofiXGB in models:
                test_preds.append(ofiXGB.predict(X_test))
                test_probas.append(ofiXGB.predict_proba(X_test))

            test_preds = mode(test_preds).mode[0]
            test_probas = np.mean(test_probas, axis=0)
        else:
            test_preds = ofiXGB.predict(X_test)
            test_probas = ofiXGB.predict_proba(X_test)

        test_auc = roc_auc_score(y_test, test_probas)
        test_f1 = f1_score(y_test, test_preds)
        test_c_matrix = confusion_matrix(y_test, test_preds)

        print(f"[TEST] AUC SCORE: {test_auc}")
        print(f"[TEST] f1 SCORE: {test_f1}")
        print("[TEST]", test_c_matrix)

        experiment_results["test"] = {
            "auc": test_auc,
            "f1": test_f1,
            "confusion_matrix": test_c_matrix.tolist(),
            "probas": test_probas.tolist(),
            "targets": y_test.tolist(),
        }

        logging.info("DONE: TESTING")

    experiment_file_name = f"{start_time_string}-xgb-{P.experiment_name}.json"

    with open(P.out_path / "results" / experiment_file_name, "w") as outfile:
        json.dump(experiment_results, outfile)

    logging.info(f"DONE: XGB. SAVED RESULTS TO {experiment_file_name}")
