#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import time
from argparse import ArgumentParser
from typing import List
from tqdm import tqdm

from pytorch_lightning import seed_everything
import scipy.stats as stats
from sklearn.metrics import confusion_matrix, f1_score, roc_auc_score
from sklearn.utils import resample

from ofiidentifier.data.ofi_dataset import OFIDataset
from ofiidentifier.models.autogluon import OFIAG
from ofiidentifier.utils.argparse_types import dir_path, file_path, path
from ofiidentifier.utils.logging import logging


def process_codes(ais: List[str], icd: List[str], pac: List[str]):
    return [f"ais_{x}" for x in ais] + [f"icd_{x}" for x in icd] + [f"pac_{x}" for x in pac]


features = [
    "pt_age_yrs",
    "pt_Gender",
    "inj_dominant",
    "inj_mechanism",
    "inj_intention",
    "pt_asa_preinjury",
    "pre_card_arrest",
    "pre_gcs_sum",
    "ed_gcs_sum",
    "pre_sbp_value",
    "ed_sbp_value",
    "pre_rr_value",
    "ed_rr_value",
    "ed_be_art",
    "ed_inr",
    "hosp_vent_days",
    "hosp_los_days",
    "hosp_dischg_dest",
    "res_gos_dischg",
    "res_survival",
    # "TraumaAlarmCriteria",
    "TraumaAlarmAtHospital",
    "AlarmRePrioritised",
    "ISS",
    "dt_alarm_hosp",
    "pre_provided",
    "pre_intubated",
    # "pre_intub_type",
    "ed_intubated",
    # "ed_intub_type",
    "pre_transport",
    "ed_emerg_proc",
    "ed_emerg_proc_other",
    "ed_tta",
    "host_transfered",
    "host_care_level",
    "dt_alarm_scene",
    "dt_ed_first_ct",
    "dt_ed_emerg_proc",
    "NumberOfActions",
    "NumberOfInjuries",
    "host_vent_days_NotDone",
    "FirstTraumaDT_NotDone",
    "filter_*",
    # "pac_*",
    # "icd_*",
    # "ais_*",
    # "Tr_Niva",
    # "iva_dagar_n",
    # "iva_vardtillfallen_n",
    # "waran_beh_vid_ank",
    # "noak_vid_ankomst",
    # "VK_hlr_thorak",
    # "VK_sap_less90",
    # "VK_leverskada",
    # "VK_gcs_less9_ej_intubTE",
    # "VK_mjaltskada",
    # "VK_mer_30min_DT",
    # "VK_mass_transf",
    # "VK_mer_60min_interv",
    # "VK_iss_15_ej_iva",
    # "VK_ej_trombrof_TBI_72h",
    # "VK_iss_15_ej_TE",
    # "ofi",
]

# good codes
ais = [310402.1, 910400.1, 650432.2, 853151.3, 650620.2, 450203.3]

icd = ["S37.00", "S27.00", "S14.0", "S22.40", "T14.0", "S72.10", "S06.0"]

pac = ["TNG32", "JAH00", "NFJ09", "TNC32"]

features_raw = process_codes(ais, icd, pac)

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("experiment_name", type=str)
    parser.add_argument("--ofi-dataset-path", default="/data/processed/ofi_train.csv", type=file_path)
    parser.add_argument("--test-dataset-path", default="/data/processed/ofi_test.csv", type=file_path)
    parser.add_argument("--cv", dest="cv", action="store_true", default=False)
    parser.add_argument("--hyper-opt-time", type=int, default=60 * 60 * 24)
    parser.add_argument("--load-model", type=path, default=None)
    parser.add_argument("--out-path", type=dir_path, default="/app/out")
    parser.add_argument("--save", dest="save", action="store_true", default=False)
    parser.add_argument("--seed", type=int, default=2022)
    parser.add_argument("--test", dest="test", action="store_true", default=False)

    P = parser.parse_args()
    P.experiment_name = P.experiment_name.replace(" ", "_")

    seed_everything(P.seed, workers=True)

    logging.info(f"STARING: AUTOGLUON. EXPERIMENT NAME: {P.experiment_name}")
    start_time_string = time.strftime("%m%d-%H%M")

    dataset = OFIDataset(P.ofi_dataset_path, features=features, features_raw=features_raw)

    experiment_results = {
        "model": "AUTOGLUON",
        "start_time": start_time_string,
        "experiment_name": P.experiment_name,
        "columns": dataset.features,
    }

    ofiAG = OFIAG(seed=P.seed)

    if P.load_model is not None:
        ofiAG.load(P.load_model)
    else:
        logging.info(f"STARTING: HYPER OPTIMISATION AND TRAINING")
        ofiAG.hyper_opt(
            dataset, time=P.hyper_opt_time, save_path=P.out_path / "models" / f"{start_time_string}-autogluon" if P.save else None
        )

        if P.save:
            ofiAG.save()

        logging.info(f"FINISHED: HYPER OPTIMISATION AND TRAINING")

    if P.cv:
        logging.info(f"STARTING: CROSS VALIDATION")
        train_data, cv_targets = dataset.train_ds()
        cv_preds = ofiAG.model.get_oof_pred(train_data=train_data)
        cv_probas = ofiAG.model.get_oof_pred_proba(train_data=train_data).to_numpy()[:, 1]

        cv_auc = roc_auc_score(cv_targets, cv_probas)
        cv_f1 = f1_score(cv_targets, cv_preds)
        cv_c_matrix = confusion_matrix(cv_targets, cv_preds)

        print(f"[CV] AUC SCORE: {cv_auc}")
        print(f"[CV] f1 SCORE: {cv_f1}")
        print("[CV]", cv_c_matrix)

        experiment_results["cv"] = {
            "auc": cv_auc,
            "f1": cv_f1,
            "confusion_matrix": cv_c_matrix.tolist(),
            "probas": cv_probas.tolist(),
            "targets": cv_targets.tolist(),
        }

        logging.info("DONE: CROSS VALIDATION")

    if P.test:
        logging.info(f"STARTING: TESTING")

        test_dataset = OFIDataset(P.test_dataset_path, features=features, features_raw=features_raw)
        X_test, y_test = test_dataset.test_df()

        test_preds = ofiAG.predict(X_test)
        test_probas = ofiAG.predict_proba(X_test)

        test_auc = roc_auc_score(y_test, test_probas)
        test_f1 = f1_score(y_test, test_preds)
        test_c_matrix = confusion_matrix(y_test, test_preds)

        print(f"[TEST] AUC SCORE: {test_auc}")
        print(f"[TEST] f1 SCORE: {test_f1}")
        print("[TEST]", test_c_matrix)


        experiment_results["test"] = {
            "auc": test_auc,
            "f1": test_f1,
            "confusion_matrix": test_c_matrix.tolist(),
            "probas": test_probas.tolist(),
            "targets": y_test.tolist(),
        }

        logging.info("DONE: TESTING")

    experiment_file_name = f"{start_time_string}-autogluon-{P.experiment_name}.json"

    with open(P.out_path / "results" / experiment_file_name, "w") as outfile:
        json.dump(experiment_results, outfile)

    logging.info(f"DONE: AUTOGLUON. SAVED RESULTS TO {experiment_file_name}")
