#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import os
import time
from argparse import ArgumentParser
from typing import List

import numpy as np
from pytorch_lightning import seed_everything
import scipy.stats as stats
from sklearn.metrics import confusion_matrix, f1_score, roc_auc_score
from sklearn.utils import resample
from tqdm import tqdm

from ofiidentifier.data.ofi_dataset import OFIDataset
from ofiidentifier.models.lgb import OFILGB
from ofiidentifier.utils.argparse_types import dir_path, file_path, path
from ofiidentifier.utils.logging import logging


def process_codes(ais: List[str], icd: List[str], pac: List[str]):
    return [f"ais_{x}" for x in ais] + [f"icd_{x}" for x in icd] + [f"pac_{x}" for x in pac]


features = [
    "pt_age_yrs",
    "pt_Gender",
    "inj_dominant",
    "inj_mechanism",
    "inj_intention",
    "pt_asa_preinjury",
    "pre_card_arrest",
    "pre_gcs_sum",
    "ed_gcs_sum",
    "pre_sbp_value",
    "ed_sbp_value",
    "pre_rr_value",
    "ed_rr_value",
    "ed_be_art",
    "ed_inr",
    "hosp_vent_days",
    "hosp_los_days",
    "hosp_dischg_dest",
    "res_gos_dischg",
    "res_survival",
    # "TraumaAlarmCriteria",
    "TraumaAlarmAtHospital",
    "AlarmRePrioritised",
    "ISS",
    "dt_alarm_hosp",
    "pre_provided",
    "pre_intubated",
    # "pre_intub_type",
    "ed_intubated",
    # "ed_intub_type",
    "pre_transport",
    "ed_emerg_proc",
    "ed_emerg_proc_other",
    "ed_tta",
    "host_transfered",
    "host_care_level",
    "dt_alarm_scene",
    "dt_ed_first_ct",
    "dt_ed_emerg_proc",
    "NumberOfActions",
    "NumberOfInjuries",
    "host_vent_days_NotDone",
    "FirstTraumaDT_NotDone",
    "filter_*",
    # "pac_*",
    # "icd_*",
    # "ais_*",
    # "Tr_Niva",
    # "iva_dagar_n",
    # "iva_vardtillfallen_n",
    # "waran_beh_vid_ank",
    # "noak_vid_ankomst",
    # "VK_hlr_thorak",
    # "VK_sap_less90",
    # "VK_leverskada",
    # "VK_gcs_less9_ej_intubTE",
    # "VK_mjaltskada",
    # "VK_mer_30min_DT",
    # "VK_mass_transf",
    # "VK_mer_60min_interv",
    # "VK_iss_15_ej_iva",
    # "VK_ej_trombrof_TBI_72h",
    # "VK_iss_15_ej_TE",
    # "ofi",
]

# good codes
ais = [310402.1, 910400.1, 650432.2, 853151.3, 650620.2, 450203.3]

icd = ["S37.00", "S27.00", "S14.0", "S22.40", "T14.0", "S72.10", "S06.0"]

pac = ["TNG32", "JAH00", "NFJ09", "TNC32"]

features_raw = process_codes(ais, icd, pac)

best_hyper_opts = {
    "boosting_type": "dart",
    "class_weight": None,
    "colsample_bytree": 0.7789575510130504,
    "importance_type": "split",
    "learning_rate": 0.009853572626710608,
    "max_depth": 2,
    "min_child_samples": 20,
    "min_child_weight": 3,
    "min_split_gain": 0.0,
    "n_estimators": 2000,
    "n_jobs": 8,
    "num_leaves": 85,
    "objective": "binary",
    "random_state": None,
    "reg_alpha": 0.9860750442245387,
    "reg_lambda": 2.6711193274538068,
    "silent": "warn",
    "subsample": 0.7129940013643866,
    "subsample_for_bin": 200000,
    "subsample_freq": 0,
    "max_delta_step": 0,
    "scale_pos_weight": 33.99261992619926,
    "seed": 2022,
}


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("experiment_name", type=str)
    parser.add_argument(
        "--ofi-dataset-path", default="/data/processed/ofi_train.onehotencoded.imputed.standardised.csv", type=file_path
    )
    parser.add_argument(
        "--test-dataset-path",
        default="/data/processed/ofi_test.onehotencoded.imputed.standardised.csv",
        type=path,
    )
    parser.add_argument("--cv", dest="cv", action="store_true", default=False)
    parser.add_argument("--hyper-opt", dest="hyper_opt", action="store_true", default=False)
    parser.add_argument("--load-model", type=path, default=None)
    parser.add_argument("--max-evals", type=int, default=150)
    parser.add_argument("--out-path", type=dir_path, default="/app/out")
    parser.add_argument("--save", dest="save", action="store_true", default=False)
    parser.add_argument("--seed", type=int, default=2022)
    parser.add_argument("--test", dest="test", action="store_true", default=False)

    P = parser.parse_args()
    P.experiment_name = P.experiment_name.replace(" ", "_")

    seed_everything(P.seed, workers=True)

    logging.info(f"STARING: LGB. EXPERIMENT NAME: {P.experiment_name}")
    start_time_string = time.strftime("%m%d-%H%M")

    dataset = OFIDataset(P.ofi_dataset_path, features=features, features_raw=features_raw)

    experiment_results = {
        "model": "LGB",
        "start_time": start_time_string,
        "experiment_name": P.experiment_name,
        "columns": dataset.features,
    }

    ofiLGB = OFILGB(seed=P.seed)
    models = None
    refit = True

    if P.hyper_opt:
        logging.info(f"STARTING: HYPER OPTIMISATION")
        best_hyper_opts = ofiLGB.hyper_opt(dataset, max_evals=P.max_evals, scale_pos_weight=dataset.scale_pos_weight)
        print("Best hyper opts:", best_hyper_opts)

        best_hyper_opts["seed"] = int(best_hyper_opts["seed"])
        experiment_results["best_hyper_opts"] = best_hyper_opts

        if P.save:
            model_path = P.out_path / "models" / f"{start_time_string}-lgb-complete.pickle"
            ofiLGB.save(model_path)

        logging.info(f"FINISHED: HYPER OPTIMISATION")
    elif P.load_model is not None:
        refit = False
        if os.path.isdir(P.load_model):
            models = []
            model_names = os.listdir(P.load_model)
            model_names.sort()

            for model_name in model_names:
                ofiLGB = OFILGB(seed=P.seed)
                ofiLGB.load(P.load_model / model_name)
                models.append(ofiLGB)
        else:
            ofiLGB.load(P.load_model)

        dataset = OFIDataset(P.ofi_dataset_path, features_raw=ofiLGB.model.feature_name_)

    else:
        ofiLGB.load_from_params(best_hyper_opts)

    if P.cv:
        logging.info(f"STARTING: CROSS VALIDATION")

        cv_models = []
        cv_targets = []
        cv_preds = []
        cv_probas = []

        for fold in tqdm(range(dataset.n_folds)):
            X_train, y_train = dataset.train_df(fold)

            if models is not None:
                ofiLGB = models[fold]

            if refit:
                ofiLGB = OFILGB(seed=P.seed)
                ofiLGB.load_from_params(best_hyper_opts)
                ofiLGB.fit(X_train, y_train)

            X_val, y_val = dataset.val_df(fold)

            fold_preds = ofiLGB.predict(X_val)
            fold_proba = ofiLGB.predict_proba(X_val)

            cv_targets = np.append(cv_targets, y_val)
            cv_preds = np.append(cv_preds, fold_preds)
            cv_probas = np.append(cv_probas, fold_proba)

            cv_models.append(ofiLGB)

            if P.save:
                model_dir = P.out_path / "models" / f"{start_time_string}-lgb"

                model_dir.mkdir(parents=True, exist_ok=True)

                ofiLGB.save(model_dir / f"{fold}.pickle")

        models = cv_models

        cv_auc = roc_auc_score(cv_targets, cv_probas)
        cv_f1 = f1_score(cv_targets, cv_preds)
        cv_c_matrix = confusion_matrix(cv_targets, cv_preds)

        print(f"[CV] AUC SCORE: {cv_auc}")
        print(f"[CV] f1 SCORE: {cv_f1}")
        print("[CV]", cv_c_matrix)

        experiment_results["cv"] = {
            "auc": cv_auc,
            "f1": cv_f1,
            "confusion_matrix": cv_c_matrix.tolist(),
            "probas": cv_probas.tolist(),
            "targets": cv_targets.tolist(),
        }

        logging.info("DONE: CROSS VALIDATION")

    if P.test:
        logging.info(f"STARTING: TESTING")

        test_dataset = OFIDataset(P.test_dataset_path, features_raw=dataset.features)
        X_test, y_test = test_dataset.test_df()

        if models is not None:
            test_preds = []
            test_probas = []
            for ofiLGB in models:
                test_preds.append(ofiLGB.predict(X_test))
                test_probas.append(ofiLGB.predict_proba(X_test))

            test_preds = stats.mode(test_preds).mode[0]
            test_probas = np.mean(test_probas, axis=0)
        else:
            test_preds = ofiLGB.predict(X_test)
            test_probas = ofiLGB.predict_proba(X_test)

        test_auc = roc_auc_score(y_test, test_probas)
        test_f1 = f1_score(y_test, test_preds)
        test_c_matrix = confusion_matrix(y_test, test_preds)

        print(f"[TEST] AUC SCORE: {test_auc}")
        print(f"[TEST] f1 SCORE: {test_f1}")
        print("[TEST]", test_c_matrix)

        experiment_results["test"] = {
            "auc": test_auc,
            "f1": test_f1,
            "confusion_matrix": test_c_matrix.tolist(),
            "probas": test_probas.tolist(),
            "targets": y_test.tolist(),
        }

        logging.info("DONE: TESTING")

    experiment_file_name = f"{start_time_string}-lgb-{P.experiment_name}.json"

    with open(P.out_path / "results" / experiment_file_name, "w") as outfile:
        json.dump(experiment_results, outfile)

    logging.info(f"DONE: LGB. SAVED RESULTS TO {experiment_file_name}")
