#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
from argparse import ArgumentParser

import pandas as pd
from pytorch_lightning import seed_everything
from sklearn.model_selection import StratifiedKFold, train_test_split

from ofiidentifier.data.ofi_processor import OFIProcessor
from ofiidentifier.utils.argparse_types import dir_path, file_path
from ofiidentifier.utils.logging import logging

if __name__ == "__main__":
    parser = ArgumentParser(description="Split OFI dataset into test and training.")
    parser.add_argument(
        "--ofi-dataset-path",
        default="/data/raw/combined_dataset.csv",
        type=file_path,
        help="the path to the ofi dataset",
    )
    parser.add_argument(
        "--output-path", default="/data/processed", type=dir_path, help="the path to output the split datasets"
    )
    parser.add_argument("--split-size", default=0.8, type=float, help="the train/test split fraction")
    parser.add_argument("--folds", type=int, default=5)
    parser.add_argument("--seed", type=int, default=2022)
    parser.add_argument("--dont-impute", dest="dont_impute", action="store_true", default=False)
    parser.add_argument("--one-hot-encode", dest="one_hot_encode", action="store_true", default=False)
    parser.add_argument("--rescale", type=str, default="standardise", choices=["standardise", "normalise", "none"])

    P = parser.parse_args()

    seed_everything(P.seed, workers=True)

    logging.info(f"STARTING: PREPROCESSING AND SPLITTING")

    df_dataset_raw = pd.read_csv(P.ofi_dataset_path)

    ofi_processor = OFIProcessor(
        df_dataset_raw, impute=not P.dont_impute, do_one_hot_encode=P.one_hot_encode, rescale=P.rescale
    )

    df_ofi_dataset = ofi_processor.df_processed
    df_ofi_dataset_raw = ofi_processor.df_raw

    nan_columns = df_ofi_dataset.columns[df_ofi_dataset.isna().any()].tolist()
    if len(nan_columns) > 0:
        logging.warning("NAN COLUMNS EXIST: " + ", ".join(nan_columns))

    df_train, df_test = train_test_split(
        df_ofi_dataset, test_size=0.2, random_state=P.seed, stratify=df_ofi_dataset["ofi"]
    )
    df_train_raw = df_ofi_dataset_raw.drop(df_test.index)

    skf = StratifiedKFold(P.folds, shuffle=True, random_state=P.seed)
    df_train["fold"] = -1

    for i, (train_idx, valid_idx) in enumerate(skf.split(df_train, df_train["ofi"])):
        df_train.iloc[valid_idx, df_train.columns.get_loc("fold")] = i

    extension = ""
    if P.one_hot_encode:
        extension += ".onehotencoded"

    if not P.dont_impute:
        extension += ".imputed"

    if P.rescale != "none":
        extension += f".{P.rescale}d"

    df_train.to_csv(P.output_path / f"ofi_train{extension}.csv")
    df_train_raw.to_csv(P.output_path / "ofi_train_raw.csv")
    df_test.to_csv(P.output_path / f"ofi_test{extension}.csv")

    with open(P.output_path / "category_columns.json", "w") as outfile:
        json.dump(ofi_processor.categories, outfile)

    logging.info(f"DONE: PREPROCESSING AND SPLITTING")
