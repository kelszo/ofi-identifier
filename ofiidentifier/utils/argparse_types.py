import os
from pathlib import Path


def path(string: str) -> Path:
    if os.path.isfile(string) or os.path.isdir(string):
        return Path(string)
    else:
        raise FileNotFoundError(string)


def file_path(string: str) -> Path:
    if os.path.isfile(string):
        return Path(string)
    else:
        raise FileNotFoundError(string)


def dir_path(string: str) -> Path:
    if os.path.isdir(string):
        return Path(string)
    else:
        raise NotADirectoryError(string)
