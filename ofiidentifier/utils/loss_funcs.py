from sklearn.metrics import f1_score, roc_auc_score


def f1_loss(target, pred):
    return 1 - f1_score(target, pred)


def roc_loss(target, pred):
    return 1 - roc_auc_score(target, pred)
