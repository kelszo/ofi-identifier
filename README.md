# ofi-identifier

## Project Structure

```
├── conf                # Configuration files
├── data                # Data files
│   ├── external        # External data
│   ├── interim         # Intermediate processed data
│   ├── processed       # Processed data
│   └── raw             # Raw data (read only)
├── ofiidentifier       # Main project source code
│   ├── data            # Code that handles data processing
│   ├── executors       # Scripts that run code end-to-end
│   ├── models
│   ├── networks        # Interchangeable networks that the models rely on
│   └── utils           # Utils than can be used in several places
├── local               # A non-pushed dir for local work
├── models              # Compiled models
├── notebooks           # Notebooks used for literate programming and executors
├── references          # Manuals and other material
├── reports
└── results             # Results from training

```

## TODO

- Run everything 24h
- ResNet/EffecientNet/CNNs
- Ensemble

## Notes

```
# Good codes with diff
# ais = [310402.1, 910400.1, 450203.3, 442205.3, 210202.1, 856151.2, 510202.1 ]

# icd = ["S37.00", "S22.40", "S06.0", "S27.00", "T14.0", "S32.80", "S13.4"]

# pac = ["NAG79", "QDB05", "QAB00", "TNG32"]

# all codes
# ais =  [140651.3, 140652.4, 140682.3, 140694.2, 150202.3, 310402.1, 441407.2, 441411.3, 442200.3, 442205.3, 450203.3, 450804.2, 650420.2, 650432.2, 650617.2, 650620.2, 853151.3, 854471.2, 856163.4, 910400.1]

# icd =  ['S02.10', 'S06.0', 'S06.3', 'S06.4', 'S06.5', 'S06.6', 'S10.8', 'S10.9', 'S14.0', 'S22.0', 'S22.20', 'S22.40', 'S27.00', 'S27.20', 'S27.30', 'S32.0', 'S32.70', 'S36.00', 'S37.00', 'S42.10', 'S72.10', 'S82.4', 'T14.0']

# pac =  ['AAA20', 'DQ023', 'GAA10', 'GBB00', 'JAH00', 'NAG79', 'NCJ19', 'NCJ69', 'NEJ69', 'NFJ09', 'NFJ59', 'NGJ09', 'NGJ29', 'QCB05', 'QDB05', 'TNB30', 'TNC32', 'TNH32']

```
