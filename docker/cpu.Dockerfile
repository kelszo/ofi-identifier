FROM python:3.9

ENV DEBIAN_FRONTEND noninteractive

WORKDIR /app

RUN apt-get update && apt-get -y upgrade

RUN apt-get install -y \
    graphviz \
    swig \
    libgl1 \
    tini  \
    wget 

COPY requirements.txt .

RUN pip install --upgrade --no-cache-dir --extra-index-url https://download.pytorch.org/whl/cpu -r requirements.txt torch

COPY jupyter_notebook_config.json .
EXPOSE 8080
ENTRYPOINT ["/usr/bin/tini", "--"]

ENV PYTHONPATH "${PYTHONPATH}:/lib/python3/"

CMD ["jupyter", "notebook", "--config=./jupyter_notebook_config.json"]