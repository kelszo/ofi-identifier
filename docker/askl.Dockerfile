FROM python:3.9

ENV DEBIAN_FRONTEND noninteractive

WORKDIR /app

RUN apt-get update && apt-get -y upgrade

RUN apt-get install -y \
    graphviz \
    swig \
    libgl1 \
    tini  \
    wget 

RUN pip install --upgrade --no-cache-dir \
    auto-sklearn \
    graphviz \
    ipywidgets \
    joblib \
    jupyter\
    numpy \
    pandas \
    pytorch-lightning \
    scikit-plot \
    seaborn \
    tqdm \
    watermark 

COPY jupyter_notebook_config.json .
EXPOSE 8080
ENTRYPOINT ["/usr/bin/tini", "--"]

ENV PYTHONPATH "${PYTHONPATH}:/lib/python3/"

CMD ["jupyter", "notebook", "--config=./jupyter_notebook_config.json"]