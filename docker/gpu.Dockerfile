FROM nvcr.io/nvidia/pytorch:22.05-py3

ENV DEBIAN_FRONTEND noninteractive

WORKDIR /app

RUN apt-get update && apt-get -y update

RUN apt-get install -y \
    graphviz \
    swig \
    libgl1 \
    tini  \
    wget 

COPY requirements.txt .

RUN pip install --upgrade --no-cache-dir pip \
    && pip install --upgrade --no-cache-dir -r requirements.txt opencv-python==4.5.5.64

COPY jupyter_notebook_config.json .
EXPOSE 8080
ENTRYPOINT ["/usr/bin/tini", "--"]

ENV PYTHONPATH "${PYTHONPATH}:/lib/python3/"

CMD ["jupyter", "notebook", "--config=./jupyter_notebook_config.json"]