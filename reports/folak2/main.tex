\documentclass{mod-comjnl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{csquotes}
\usepackage{hyperref}
\usepackage{url}
\usepackage{siunitx}
\usepackage[gen]{eurosym}
\usepackage[nopostdot,style=super,nonumberlist,toc,nogroupskip,acronym]{glossaries}
\makenoidxglossaries
\usepackage{blindtext}
\usepackage{booktabs}
\usepackage{pgf}
\usepackage[tableposition=top,font=small]{caption}
\usepackage{subcaption}
\usepackage{float}
\usepackage{stfloats}
\usepackage[top=2cm, left=1.65cm, right=1.65cm, bottom=0.75cm]{geometry}
\usepackage[flushmargin]{footmisc}
\usepackage{xcolor}
\usepackage{multirow}
\usepackage{tikz}

\hypersetup{hidelinks}

% Tikz setup
\usetikzlibrary{shapes, arrows}
\tikzstyle{terminator} = [rectangle, draw, text centered, rounded corners, minimum height=2em, font=\footnotesize]
\tikzstyle{process} = [rectangle,text width=3cm, minimum height=0.75cm, text centered, draw=black, font=\footnotesize]
\tikzstyle{decision} = [diamond, text centered, draw=black, font=\footnotesize]
\tikzstyle{arrow} = [thick,->,>=stealth]

\makeatletter
\setlength{\@fptop}{0pt}
\makeatother

%% These two lines are needed to get the correct paper size
%% in TeX Live 2016
\let\pdfpageheight\paperheight
\let\pdfpagewidth\paperwidth

% Glossary and acronyms
%\glsdisablehyper
%\newacronym{who}{WHO}{World Health Organization}
\newacronym{mam}{M\&M}{morbidity and mortality}
\newacronym{qi}{QI}{quality improvement}
\newacronym{ofi}{OFI}{opportunity for improvement}
\newacronym{kuh}{KUH}{Karolinska University Hospital}
\newacronym{mlp}{MLP}{multilayer perceptron}
\newacronym{auc}{AUC}{area under the receiver operating characteristic curve}
\newacronym{ml}{ML}{machine learning}
\newacronym{icd}{ICD}{international classification of diseases}
\newacronym{pac}{PAC}{activity code from the Swedish National Board of Health and Welfare}
\newacronym{ais}{AIS}{abbreviated injury scale}

%\renewcommand{\glsnamefont}[1]{\textbf{#1}}

\newglossarystyle{csuper}{%
\setglossarystyle{super}%
  \renewcommand{\glossentry}[2]{%
    \glsentryitem{##1}\glstarget{##1}{\glossentryname{##1}} &
    \Glossentrydesc{##1}\glspostdescription\space ##2\tabularnewline
  }
\setlength{\glsdescwidth}{0.75\linewidth}
}


\sisetup{
  group-four-digits = true,
  group-separator = {,}
}

% custom vancouver styling
\input{vancouver.tex}

\addbibresource{main.bib}
 
\begin{document}

\title[Predicting OFIs in Trauma Patient Care Using ML]{Predicting Opportunity for Improvement in Trauma Patient Care Using Machine Learning}
\author{Kelvin Szolnoky}
\shortauthors{K. Szolnoky}
\affiliation{Department of Global Public Health,\\ Karolinska Institutet,\\ Stockholm, Sweden}
\email{kelvin.szolnoky@ki.se}
\supervisor{Martin Gerdin Wärnberg \& Jonatan Attergrim}

\begin{abstract}
  Patient care quality improvement systems are a cornerstone in reducing the global impact that trauma has on mortality and morbidity. However, these systems can be very costly and resource ineffective due to the number of cases that must manually be reviewed by a multidisciplinary conference. Currently, some programmes use audit filters to reduce the amount of false positive cases brought to reviews. Yet these filters still suffer from a considerable amount of false positives. The current study investigated the performance of modern \acrfull{ml} models vs. current-day audit filters at \acrfull{kuh} in Sweden at identifying \acrfull{ofi} in trauma patient care. 11,854 trauma cases (whereof 339 with previously identified \acrshort{ofi}) were retrieved from the Swedish trauma registry (SweTrau) and \acrshort{kuh}'s quality trauma registry. Three gradient boosting, two traditional ensemble, and two neural network-based models were trained on 80\% of the data and evaluated on the remaining 20\% using ROC-AUC. The results showed that the top performing \acrshort{ml} model (\textit{AutoGluon}) scored 0.833 AUC on the test set vs. the current-day audit filters that scored 0.754. The findings indicate that it is possible to design \acrshort{ml} models to replace current-day audit filters to improve performance in identifying \acrshortpl{ofi} and thus reduce costs of quality improvement systems.
\end{abstract}

\maketitle
\glsaddall
\printnoidxglossary[type=acronym,style=csuper]

\section{Introduction}
Trauma, the result of physical injury, is one of the leading causes of morbidity and mortality worldwide and the leading cause of death of young people in Sweden \cite{sos_death_2021,roth_global_2018}. The trauma population has a low average age and two-thirds of the group have no history of co-morbidity \cite{brattstrom_socio-economic_2015}. Thus it is essential to ensure a constant high-quality trauma care to lower mortality and morbidity for this population with long a age expectancy.

A method of improving trauma patient care is to implement \acrfull{qi} programmes, as suggested by the World Health Organization (WHO) \cite{world_health_organization_guidelines_2009}. \Acrfull{mam} conferences are a cornerstone in such programmes, though, are heavily resource intensive. A more advanced \acrshort{qi} technique suggested by WHO is the application of audit filters that use medical record systems to monitor predefined variables to flag cases with \acrfull{ofi}. Filtering cases using an audit filter before reviewing by an \acrshort{mam} conference provides a possibility to reduce false positive cases and thus reducing the costs of the \acrshort{qi} programme as a whole.

The \acrfull{kuh} in Sweden is a level-I trauma centre with a catchment area of approximately 2 million inhabitants \cite{brattstrom_time_2012}. \acrshort{kuh} currently uses system to screen and exclude cases without \acrshortpl{ofi} before including them at an \acrshort{mam}. In essence, the system works by a nurse reviewing a trauma case and adding appropriate information to the Swedish trauma register (SweTrau). While doing this they take note if the case activates any audit filters (see \ref{appendix:auditfilters} for a complete list of filters) and registers these into a separate quality register at \acrshort{kuh}. If any audit filter is flagged as positive, two nurses review the patients' electronic health record and determine if \acrshort{ofi} is possible. Lastly, if the nurses determine \acrshort{ofi} may be possible for the case it is brought up at the next \acrshort{mam} conference to decisively determine if there is \acrshort{ofi}. All cases that result in a patients death are brought up at the \acrshort{mam} conference.

\begin{figure}[]
  \centering
  \begin{tikzpicture}[node distance=1.5cm]
    \node [terminator] (start) {\textbf{Trauma registry}};
    \node (auditfilter) [process, below of=start] {Audit filter/Review by a nurse};
    \node (twonursereview) [process, below of=auditfilter] {Review by two nurses};
    \node (conference) [process, below of=twonursereview] {\acrshort{mam} conference};
    \node (ofi) [terminator, below of=conference] {\textbf{OFI}};
    \node (noofi) [terminator, right of=ofi, xshift=1cm] {\textbf{No OFI}};

    \node (death) [decision, left of=start, xshift=-1.5cm] {Death};

    \draw [arrow] (start) -- node [left, xshift=-0.15cm] {\footnotesize $n=11846$} (auditfilter);
    \draw [arrow] (auditfilter) -- node  [left, xshift=-0.15cm] {\footnotesize $n=3675$} (twonursereview);
    \draw [arrow] (twonursereview) --  node  [left, xshift=-0.15cm] {\footnotesize $n=1260$} (conference);
    \draw [arrow] (conference) -- node  [left, xshift=-0.15cm] {\footnotesize $n=340$} (ofi);

    \draw [arrow] (start) -- (death);
    \draw [arrow] (death) |- node [left, xshift=-0.15cm] {\footnotesize $n=356$} (conference);

    \draw [arrow] (auditfilter) -| node  [right, xshift=0.15cm] {\footnotesize $n=7185$} (noofi);
    \draw [arrow] (twonursereview) -| node  [right, xshift=0.15cm] {\footnotesize $n=2415$} (noofi);
    \draw [arrow] (conference) -| node  [right, xshift=0.15cm] {\footnotesize $n=1276$} (noofi);

    \node[right of=noofi, yshift=0.85cm, xshift=-0.55cm] {\footnotesize $n=11506$};

  \end{tikzpicture}
  \caption{A summarised flowchart of the fundamental way for identifying opportunities for improvement in trauma patient care at Karolinska University Hospital in Sweden.} \label{fig:ofiflow}
\end{figure}

Even so, the current system produces a large amount of false positive \acrshort{ofi} flags with roughly three-quarters of cases being called to an \acrshort{mam} resulting in not having an \acrshort{ofi}. Thus, improvements must be made to reduce unnecessary cases being brought to the \acrshort{mam} conferences. A rising trend in medical research is using \acrfull{ml} to automate resource-heavy tasks. In many cases, the replacements have resulted in on-par or even state-of-the-art results compared to the current gold standard \cite{pham_ai_2021,bulten_artificial_2022}. Up until now, no study has been done to evaluate the application of \acrshort{ml} in identifying \acrshortpl{ofi} in patient care.

An \acrshort{ml} based system can provide many benefits over current day systems for detecting \acrshort{ofi}. Firstly, they could reduce the resources needed for implementing and running a \acrshort{qi} programme. Secondly, it's possible that such systems could improve performance and lessen the amount of false positive cases brought to an \acrshort{mam} conference. Thirdly, they introduce a possibility for active learning. It has been suggested that audit filter systems lose effectiveness and need to be periodically evaluated and updated when their goals are reached \cite{cryer_continuous_1996}. \Acrshort{ml}-based systems provide a base to actively learn from each new case, thus not requiring the same evaluation and degradation of effectiveness.

The primary aim of the present study is to design and analyse the performance of an \acrshort{ml} model as a replacement for the current audit filter step in identifying opportunities for improvement in trauma patient care at Karolinska University Hospital in Sweden.



\section{Materials and Methods}
The code used in this study is publicly available online at \url{https://codeberg.org/kelszo/ofi-identifier} under the \textit{GNU Affero General Public License v3.0}.

\subsection{Study Population and Datasets}
The models used in this study were trained on retrospective data from SweTrau and \acrshort{kuh} internal trauma quality register.

SweTrau is a Utstein template compliant register for major trauma reporting in Sweden \cite{swetrau_2021,ringdal_utstein_2008}. SweTrau includes data such as vital parameters, e.g. heart rate, at the trauma scene and admittance, as well as miscellaneous information, e.g. time to CT-scan. See the SweTrau manual \cite{swetrau_2021} for inclusion and exclusion criteria as well as a complete list of parameters for SweTrau.

Only patients admitted to \acrshort{kuh} after 2017 were included from SweTrau. No exclusions were made. Only data from SweTrau were used as features, the \acrshort{kuh} quality register data was used to create the binary \acrshort{ofi} label (yes/no) for each case and calculate the performance of current audit filters. A case was labeled as \acrshort{ofi} positive if the \acrshort{mam} conference found an \acrshort{ofi}, otherwise \acrshort{ofi} was coded as negative (even for cases never brought to the \acrshort{mam} conference).

\subsection{Data pre-processing}
Missing scalar values were imputed using the median of the dataset for that feature. Imputation of categorical values was done by introducing an unknown category. Scalar values were later rescaled using Yeo-Johnson power transform \cite{yeo_new_2000}. The \acrshort{icd}, \acrshort{pac}, and \acrshort{ais} codes with over 1\% prevalence were encoded as binary columns.

Lastly, the data was split into a stratified 80\%-20\% training-test split, the training set was stratified into five folds for use in cross-validating. The models were not evaluated on the test set until hyperparameter optimisation was complete and the models were finalised.

\subsection{Models}
\Acrshort{ml} models were selected from the top performing, widely available models in current literature. All SweTrau features were used in training and inference apart from the trauma alarm levels for the patient hospital (\texttt{TraumaAlarmCriteria}), the intubation type (\texttt{pre\_intub\_type} and \texttt{ed\_intub\_type}), and \texttt{pre\_card\_arrest} due to these carrying little to no information. \texttt{ISS} was used over \texttt{NISS} due to them carrying the same information. Only \acrshort{icd}, \acrshort{pac}, and \acrshort{ais} handpicked codes with a statistical difference between the two labels were included. All models used the same features.

Where possible, hyperparameter optimisation was utilised across the five folds for a max run-time of twenty-four hours.

\subsubsection*{Logit Regression}
A simple logit regression was used to provide a evaluation baseline for the models. The default data pre-processing was applied.

\subsubsection*{Gradient Boosting}
\label{sec:gradboost}
\textit{XGBoost} \cite{chen_xgboost_2016}, \textit{LightGBM} \cite{NIPS2017_6449f44a}, and \textit{Catboost} \cite{NEURIPS2018_14491b75} gradient boosting methods were used. For \textit{LightGBM} and \textit{XGBoost} the default data pre-processing in addition to one-hot encoding was used. However, due to \textit{CatBoost's} internals, no data pre-processing was applied and categorical embedding was used. The default suggested parameters were used for hyperparameter optimisation.

\subsubsection*{Traditional Ensembles}
Traditional \acrshort{ml} methods, e.g. SVM, random forests, kNN, etc., were used in ensembles through auto \acrshort{ml} frameworks \textit{AutoGluon} \cite{agtabular} and \textit{Auto-Sklearn} \cite{feurer-arxiv20a}. Both allowed for the automatic creation of high-performant \acrshort{ml} methods. Additionally, \textit{AutoGluon} includes the usage of neural networks and gradient boosting machines. The neural network models were disabled to only use traditional methods. However, the individual hyperparameters from \textit{XGBoost}, \textit{LightGBM}, and \textit{CatBoost} were used in \textit{AutoGluon}. No data pre-processing was done as that was handled internally for respective model.

\subsubsection*{Well-tuned Multilayer Perceptron}
Previously, neural networks have underperformed in the field of tabular data compared to their more traditional counterparts (e.g. gradient boosting). Recently, however, it has been suggested that well-tuned simple \acrfullpl{mlp} can provide state-of-the-art results \cite{kadra_well-tuned_2021}. These \acrshortpl{mlp} utilised a cocktail of different regularisation techniques such as batch normalisation, dropout, and data augmentation. The end-to-end framework \textit{Auto-Pytorch} \cite{deng-ecml22} for data pre-processing and finding well-tuned \acrshortpl{mlp} were used. To only evaluate to predicted power of \acrshortpl{mlp} the traditional ensemble pipeline was disabled. No data pre-processing was done.

\subsubsection*{TabNet}
\textit{TabNet} is a deep neural network-based architecture that uses sequential attention to choose which features to use \cite{arik_tabnet_2020}. It allows for self-supervised pre-training and data augmentation for increased performance. A model was created with a self-supervised pre-trained network and suggested hyperparameters for optimisation were used. The default data pre-processing was used, however categorical embedding was done instead of one-hot encoding.

\subsection{Ethical approval}
The study's data usage  was  approved  by  the Stockholm regional ethics committee (permits 2021-02541 and 2021-03531).

\subsection{Evaluation and Statistical Analysis}
All models were evaluated using the \acrfull{auc}. The \acrshort{auc} for the current audit filters were created by assuming if one or more filters are positive then the predicted \acrshort{ofi} probability is 100\%, else it is 0\%.

The models' final predicted \acrshort{ofi} probability was created by predicting the probability of test data points over each model's trained fold. Then soft voting was used to produce a final probability, i.e. the mean of the folds probability prediction was used as the final prediction.


\section{Results}
\begin{figure*}[]
  \begin{center}
    \renewcommand\sffamily{} % fix font
    \resizebox{0.8\textwidth}{!}{\input{figures/roc.pgf}}
  \end{center}

  \captionsetup{width=0.75\linewidth}
  \caption{Receiver operating characteristic curve for \acrshort{ofi} detection of \acrshort{ml} models vs. the baseline audit filters. The audit filter curve was created by assuming if one or more filters were positive the predicted probability for an \acrshort{ofi} was 100\%, else 0\%.}
  \label{fig:roccurve}
\end{figure*}


\begin{table}[]
  \caption{Comparison of \acrshort{ml} models vs. audit filters performance in terms of \acrfull{auc}. CV denotes the final cross-validation result using five folds. Test stands for the 20\% hold-out set created before model optimisation. \label{table:results}}
  \begin{tabular}{@{}llcc@{}}
    \toprule
    \textbf{Group}                        & \textbf{Model}   & \textbf{CV}    & \textbf{Test}  \\ \midrule
    \multirow{2}{*}{Baseline}             & Audit filter     & 0.752          & 0.754          \\
                                          & Logit regression & 0.804          & 0.819          \\ \midrule
    \multirow{3}{*}{Gradient boosting}    & CatBoost         & 0.830          & 0.825          \\
                                          & XGB              & 0.806          & 0.815          \\
                                          & LightGBM         & 0.813          & 0.810          \\ \midrule
    \multirow{2}{*}{Traditional Ensemble} & AutoGluon        & \textbf{0.838} & \textbf{0.833} \\
                                          & Auto-Sklearn     & 0.821          & 0.822          \\ \midrule
    \multirow{2}{*}{Neural Network}       & MLP              & -              & 0.830          \\
                                          & TabNet           & 0.795          & 0.827          \\ \bottomrule
  \end{tabular}
\end{table}
In total, 9483 trauma cases (271 \acrshort{ofi} positive) were included in training and 2371 cases (68 \acrshort{ofi} positive) in testing. The current audit filters scored a baseline value of 0.754 \acrshort{auc} on the test set. Logit regression had an \acrshort{auc} of 0.819 on the test set.

All \acrshort{ml} models outperformed the current-day audit filter system, with the top performing model being \textit{AutoGluon} scoring 0.833 \acrshort{auc} on the test set and cross-validated with a top score of 0.838 AUC. The gradient boosting group scored similar results with \textit{CatBoost} performing best with a score of 0.830 and 0.825 on cross-validation and the test set respectively. The neural networks had top performing scores on the test set with \acrshort{mlp} and \textit{TabNet} scoring 0.830 and 0.827 respectively. Though, \textit{TabNet} had the worse performing cross-validation and the only model that scored under 0.8 \acrshort{auc} with a final cross-validation score of 0.795 \acrshort{auc}.


\section{Discussion}
Currently, \acrshort{ofi} identification in patient care is a difficult problem and requires a substantial amount of resources. The nature of \acrshort{ofi} identification and its occasional subjectivity leads to it being an exceptionally noisy problem. Additionally, the incidence of \acrshortpl{ofi} remains relatively low with non-\acrshort{ofi} cases outweighing them by roughly 33:1. All together, this makes automating \acrshort{ofi} identification a difficult problem for \acrshort{ml} to solve.

However, here we have demonstrated that \acrshort{ml} methods can outperform current-day audit filters in detecting possible \acrshortpl{ofi} with an \acrshort{auc} of 0.833 vs. 0.754 in favour of the \acrshort{ml} model. All tested models' performance, both on cross-validation and the test set, did not have a widespread compared to each other suggesting the bottleneck of current models is not the architecture but rather the input. This is further demonstrated by the similar performance of logit regression to the \acrshort{ml} models.

The top performing model consisted of an ensemble of traditional methods (\textit{AutoGluon}), however, the \acrshort{mlp} based model, \textit{TabNet}, and \textit{CatBoost} model scored very similar results. Out of these models, \textit{CatBoost} required the least amount of resources for both training and inference and completing its best hyperparameter optimisation after only one hour. Based on the results of this study, a real world implementation should use \textit{AutoGluon}. This is due to its inclusion of several different \acrshort{ml} models (including neural networks) and relatively low resource requirement.

Though not a part of this study's scope, when using the audit filters as features the \acrshort{auc} of the models' cross-validation score rose above 0.9. Upon further investigation, it was discovered that several of the audit filters that boosted the models' performance remarkably could in theory be created by only using the SweTrau data. When examining why the models were not learning these correlations it was discovered that \acrshort{icd}, \acrshort{pac}, and \acrshort{ais} variables were highly noisy. For example, in those cases where resuscitative thoracotomy had been provided (an audit filter for finding \acrshort{ofi}) only a few cases included some sort of thoracotomy \acrshort{icd} or \acrshort{pac} code. This implies that more persistent and attentive data collection for the SweTrau register could greatly increase the models' performance. Furthermore, better handling of said codes by for instance grouping multiple codes as one feature could further improve the models' performance. Additionally, exclusion of the noisiest labels from the training set, e.g. removing data with an excessively high loss on hold-out validation, could further improve the models performance on a test set.

The current study provides a base for replacing audit filters with a robust \acrshort{ml} system, however, it does have limitations. Due to the nature of the random training-test split it is not possible to draw conclusions on the models' prospective performance for completely new \acrshortpl{ofi}. Initially, it was planned to use cases from 2021 as the test set. However, after investigating the split it was discovered that the test data was too heterogeneous to the training data and thus not representative for the \acrshort{ofi} problem as a whole. Additionally, the single-site study design limits the generalisation to other sites.

Further studies should investigate further improving performance by revamping the models' input. This includes handling \acrshort{icd}, \acrshort{pac}, and \acrshort{ais} codes in a more resourceful way and excluding noisy data. Furthermore, the addition of extra information, such as the trauma leader's self-predicted \acrshort{ofi} outcome, could greatly increase performance. Additionally, using a prospective test set to greater understand the models' performance on future data should be done.

\section{Conclusion}
In the current study, it is shown that it is possible to design machine learnings models to detect opportunity for improvement in trauma patient care that outperforms current-day audit filters. The findings indicate that implementation of machine learning systems could improve performance of opportunity for improvement detection and thus lower the implementation cost of quality improvement programmes within trauma patient care.

\printbibliography

\appendix
\section{Audit Filters}
\label{appendix:auditfilters}
The following list is of the current audit filters used to detect opportunity for improvement in trauma patient care at Karolinska University Hospital in Stockholm, Sweden:
\begin{itemize}
  \item Resuscitative thoracotomy was provided
  \item Systolic arterial pressure is less tan 90 at admittance
  \item Liver damage occurred
  \item GCS less than 9 and not intubated
  \item Spleen damage occurred
  \item More than 30 minutes to first CT-scan
  \item Mass transfusion occurred
  \item More than 60 minutes to first intervention
  \item ISS more than 15 and no intensive care
  \item Traumatic brain injury and no thrombosis prophylaxis started within 72 hours
  \item ISS more than 15 and no trauma unit activated
  \item Other findings by nurse
\end{itemize}

\end{document}
